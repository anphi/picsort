﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PicSort;
using PicSort.File;

namespace PicSortWinF
{ 
    public partial class TagEditor : Form
    {
        private UiLogic _Ui;

        public event TagBaseEventHandler NewTag;

        public TagEditor(UiLogic ui)
        {
            InitializeComponent();
            InitComponents();
            _Ui = ui;           
        }

        public TagEditor(UiLogic ui, Tag tag) : this(ui)
        {
            tbName.Text = tag.Name;
            tbKuerzel.Text = tag.Token;
            tbCustomString.Text = tag.CustomString;
            btChooseColor.BackColor = tag.Color;
            
        }
        private void InitComponents()
        {
            rbPrefix.Checked = true;
            rbCustom.Checked = true;
        }

        private Tag CreateTag()
        {
            if (tbName.Text != "<Name eingben>")
            {
                TagPosition pos = TagPosition.prefix;
                if (rbPrefix.Checked) pos = TagPosition.prefix;
                else if (rbSuffix.Checked) pos = TagPosition.suffix;
                 
                if (rbCustom.Checked)
                {
                    Tag tmp = new Tag(tbName.Text, tbKuerzel.Text, tbCustomString.Text, btChooseColor.BackColor, pos, TagType.customString);
                    return tmp;
                }
                else if (rbTimestamp.Checked)
                {
                    Tag tmp = new Tag(tbName.Text, tbKuerzel.Text,"", btChooseColor.BackColor, pos, TagType.timestamp);
                    return tmp;
                }
                else
                {
                    MessageBox.Show("Art des Tags fehlt!");
                    return null;
                }   
                        

            }
            else throw new FormatException("Pfad oder Name ungültig!");
            }

        private void tbName_Click(object sender, EventArgs e)
        {
            tbName.SelectAll();
        }

        private void tbKuerzel_Click(object sender, EventArgs e)
        {
            tbKuerzel.SelectAll();
        }
                     
        private void rbCustom_CheckedChanged(object sender, EventArgs e)
        {
            tbCustomString.Enabled = rbCustom.Checked;
        }

        private void btChooseColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            btChooseColor.BackColor = colorDialog1.Color;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            Tag tmpTag = CreateTag();
            if (tmpTag != null)
            {
                NewTag?.Invoke(this, new TagBaseEventArgs(tmpTag));
                this.Dispose();
            }
            
        }

        private void newTag_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if (_Ui.SSaveTagTargetWithEnter) btSave_Click(this, new EventArgs());
            }
        }
    }
}

