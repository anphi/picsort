﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PicSort.File;
using PicSort;
using PicSort.gui;

namespace PicSortWinF
{
    public partial class TargetEditor : Form
    {

        private UiLogic _Ui;

        public event TagBaseEventHandler NewTarget; 

        public TargetEditor(UiLogic ui)
        {
            _Ui = ui;
            InitializeComponent();
            cbEnableRenameTool_CheckedChanged(this, new EventArgs());
        }

        public TargetEditor(UiLogic ui, Target target) : this(ui)
        {
            tbName.Text = target.Name;
            tbKuerzel.Text = target.Token;
            tbPath.Text = target.Path;
            btChooseColor.BackColor = target.Color;
            tbCustomString.Text = target.CustomString;
            if (target.TargetType == TargetType.counter)
            {
                cbEnableRenameTool.Checked = true;
                rbCounter.Checked = true;
            }
            else if (target.TargetType == TargetType.customString)
            {
                rbCustom.Checked = true;
                cbEnableRenameTool.Checked = true;
            }
            else if (target.TargetType == TargetType.timestamp)
            {
                cbEnableRenameTool.Checked = true;
                rbTimestamp.Checked = true;
            }
            else if(target.TargetType == TargetType.noRename)
            {
                cbEnableRenameTool.Checked = false;
            }
  
        }

        private Target CreateTarget()
        {
            if (tbPath.Text != "<Pfad eingeben>" && tbName.Text != "<Name eingben>")
            {                             
                if (rbCounter.Checked && cbEnableRenameTool.Checked)
                {
                    Target tmp = new Target(tbName.Text, tbPath.Text, tbKuerzel.Text,"", btChooseColor.BackColor,TargetType.counter);
                    return tmp;
                }
                else if (rbCustom.Checked && cbEnableRenameTool.Checked)
                {
                    Target tmp = new Target(tbName.Text, tbPath.Text, tbKuerzel.Text, tbCustomString.Text, btChooseColor.BackColor, TargetType.customString);
                    return tmp;
                }
                else if (rbTimestamp.Checked && cbEnableRenameTool.Checked)
                {
                    Target tmp = new Target(tbName.Text, tbPath.Text, tbKuerzel.Text, "", btChooseColor.BackColor, TargetType.timestamp);
                    return tmp;
                }
                else
                {
                    Target tmp = new Target(tbName.Text, tbPath.Text, tbKuerzel.Text, "", btChooseColor.BackColor, TargetType.noRename);
                    return tmp;
                }
                
            }
            throw new FormatException("Pfad oder Name ungültig!");
        }

        private void cbEnableRenameTool_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control c in gbRename.Controls)
            {
                c.Enabled = cbEnableRenameTool.Checked;
            }
            tbCustomString.Enabled = rbCustom.Checked;
        }

        private void tbName_Click(object sender, EventArgs e)
        {
            tbName.SelectAll();
        }

        private void tbKuerzel_Click(object sender, EventArgs e)
        {
            tbKuerzel.SelectAll();
        }

        private void tbPath_Click(object sender, EventArgs e)
        {
            tbPath.SelectAll();
        }

        private void btBrowse_Click(object sender, EventArgs e)
        {
            if (_Ui.SPathTargetDefault != "") folderBrowserDialog1.SelectedPath = _Ui.SPathTargetDefault;
            folderBrowserDialog1.ShowDialog();
            tbPath.Text = folderBrowserDialog1.SelectedPath + "\\";
            _Ui.SPathTargetDefault = folderBrowserDialog1.SelectedPath;
        }

        private void rbCustom_CheckedChanged(object sender, EventArgs e)
        {
            tbCustomString.Enabled = rbCustom.Checked;
        }

        private void btChooseColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            btChooseColor.BackColor = colorDialog1.Color;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            Target tmpTarget = CreateTarget();
            NewTarget?.Invoke(this, new TagBaseEventArgs(tmpTarget));
            this.Dispose();
        }

        private void newTarget_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                if (_Ui.SSaveTagTargetWithEnter) btSave_Click(this, new EventArgs());
            }
        }
    }
}
