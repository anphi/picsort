﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
using ComponentFactory.Krypton.Ribbon;

using PicSort;
using PicSort.File;
using System.IO;

namespace PicSortWinF
{
    public partial class MainForm : KryptonForm, IPicSortUi
    {
        #region Felder

        private UiLogic _Ui;
        private bool _Reloading;
        private KrTagButton _OldTagButton;
        private KeyBindOverview _KeyBindOverview;
        
        #endregion

        #region Properties

        #endregion

        #region Events
        #endregion

        #region Konstruktor

        public MainForm()
        {
            InitializeComponent();
            _Ui = new UiLogic(this,Os.win);
            InitWindowsAsync();
            _Ui.NewPictureReady += Ui_newPictureReady;
            _Ui.NewTableReady += Ui_newTableReady;
            _Ui.TagBaseNewEvent += ui_TagBaseNewEvent;
            _Ui.TagBaseChangedEvent += ui_TagBaseChanged;
            _Ui.TagBaseDeletedEvent += Ui_TagBaseDeletedEvent;
            this.Shown += MainForm_Shown;
            _Ui.NewProgress += _Ui_newProgress;
        }

        

        #endregion

        #region Destruktor

        #endregion

        #region Methoden

        public void ZoomPicture(double factor)
        {
            pbMain.SizeMode = PictureBoxSizeMode.Zoom;
            pbMain.Width = (int)Math.Ceiling(pbMain.Width * factor);
            pbMain.Height = (int)Math.Ceiling(pbMain.Height * factor);
        }

        #endregion

        #region Hilfsmethoden

        private void OpenFolderDialog()
        {
            folderBrowser.SelectedPath = _Ui.SPathLastFolder;
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                _Ui.SPathLastFolder = folderBrowser.SelectedPath;
                _Ui.SaveSettings();
                _Ui.LoadFolderAsync(folderBrowser.SelectedPath);
                
            }
        }

        private void ChangeStatusbar()
        {
            lblPictureCount.Text = "Geladene Bilder: "+ _Ui.ImageCount.ToString();
            lblDeletedPics.Text = "Zum Löschen vorgemerkt: " + _Ui.PicsToDelete.ToString();
            lblUnsortedPics.Text = "Unsortiert: " + _Ui.UnsortedPics.ToString();
            
        }

        private void FitToScreen()
        {
            pbMain.Width = pnPicture.Width;
            pbMain.Height = pnPicture.Height;
            pbMain.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void WriteAltKeyDescriptions()
        {
            foreach (var tab in ribMain.RibbonTabs)
            {
                foreach (var group in tab.Groups )
                {
                    foreach (var item in group.Items)
                    {
                        //TODO iterate through all buttons
                    }
                } 
            }
        }

        private async void InitWindowsAsync()
        {
           await Task.Run(() => _KeyBindOverview = new KeyBindOverview(_Ui));
        }

        private void SetProgress()
        {
            pgbProgress.Value = _Ui.Progress;
            lblProgress.Text = _Ui.Progress.ToString() + '%';
        }

        private void ScrollDataGridView()
        {
            if (dgFiles.Rows.Count > 0) dgFiles.Rows[_Ui.SelectedRowIndex].Selected = true;
            if (_Ui.SelectedRowIndex > _Ui.SScrollCount && dgFiles.Rows.Count > 0)
            {
                dgFiles.FirstDisplayedScrollingRowIndex = _Ui.SelectedRowIndex - _Ui.SScrollCount;
                dgFiles.Update();
            }
            
        }

        #endregion

        #region Ereignismethoden

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _Ui.FinishStartup();
        }

        private void cmdOpenFolder_Execute(object sender, EventArgs e)
        {
            OpenFolderDialog();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _Ui.SaveSettings();
        }

        private void btZoomIn_Click(object sender, EventArgs e)
        {
            ZoomPicture(1.1);
        }

        private void btZoomOut_Click(object sender, EventArgs e)
        {
            ZoomPicture(0.9);
        }

        private void btOriginalSize_Click(object sender, EventArgs e)
        {
            pbMain.SizeMode = PictureBoxSizeMode.Normal;
            pbMain.Width = _Ui.ImageWidth;
            pbMain.Height = _Ui.ImageHeight;
        }

        private void btFitToScreen_Click(object sender, EventArgs e)
        {
            FitToScreen();
        }

        private void dgFiles_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btNext_Click(object sender, EventArgs e)
        {
            _Ui.NextFile();
            ScrollDataGridView();
        }

        private void btPrevious_Click(object sender, EventArgs e)
        {
            _Ui.PreviousFile();
            ScrollDataGridView();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            _Ui.DeleteFile();
        }

        private void btRotateLeft_Click(object sender, EventArgs e)
        {
            _Ui.Rotate(PicSort.Orientation.left);
        }

        private void btRotateRight_Click(object sender, EventArgs e)
        {
            _Ui.Rotate(PicSort.Orientation.right);
        }

        private void btOverhead_Click(object sender, EventArgs e)
        {
            _Ui.Rotate(PicSort.Orientation.head);
        }

        private void tagBaseButtonClick(object sender, EventArgs e)
        {
            var sd = (KrTagButton)sender;
            _Ui.AssignTagBase(sd.TagBase);

        }

        private void btExec_Click(object sender, EventArgs e)
        {
            _Ui.ExecuteAsync();
        }

        private void btSaveXML_Click(object sender, EventArgs e)
        {
            saveFileDialog.InitialDirectory = _Ui.SPathLastXml;
            saveFileDialog.AddExtension = true;
            saveFileDialog.Filter = "XML Datei |*.xml";
            saveFileDialog.DefaultExt = ".xml";
            saveFileDialog.FileName = "";
            DialogResult res = saveFileDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                string path = saveFileDialog.FileName;
                _Ui.SPathLastXml = Path.GetDirectoryName(path);
                _Ui.StoreXML(path);
            }

        }

        private void btLoadXml_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = _Ui.SPathLastXml;
            openFileDialog.Filter = "XML Dateien |*.xml";
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _Ui.SPathLastXml = Path.GetDirectoryName(openFileDialog.FileName);
                _Ui.LoadXML(openFileDialog.FileName);
            }

        }

        private void btAddXmlItems_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = _Ui.SPathLastXml;
            openFileDialog.Filter = "XML Dateien |*.xml";
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _Ui.SPathLastXml = Path.GetDirectoryName(openFileDialog.FileName);
                _Ui.AddTagBasefromXml(openFileDialog.FileName);
            }
        }

        private void bteditKeybinds_Click(object sender, EventArgs e)
        {
            _Ui.EditKeybinds();
        }

        private void btSettings_Click(object sender, EventArgs e)
        {
            var tmp = new SettingsEditor(_Ui);
            tmp.Show();
        }

        private void btUndo_Click(object sender, EventArgs e)
        {
            _Ui.Undo();
        }

        private void btRedo_Click(object sender, EventArgs e)
        {
            _Ui.Redo();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            _Ui.HandleKeyStroke(sender, e);
        }

        private void ändernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_OldTagButton != null)
            {
                if (_OldTagButton.TagBase.GetType() == typeof(Tag))
                {
                    var edit = new TagEditor(_Ui, (Tag)_OldTagButton.TagBase);
                    edit.NewTag += Edit_NewTag;
                    edit.Show();
                }
                else if (_OldTagButton.TagBase.GetType() == typeof(Target))
                {
                    var edit = new TargetEditor(_Ui,(Target)_OldTagButton.TagBase);
                    edit.NewTarget += Edit_NewTag;
                    edit.Show();
                }
            }

        }

        private void Edit_NewTag(object sender, TagBaseEventArgs EventArgs)
        {
            _Ui.EditTagBase(_OldTagButton.TagBase.Name, EventArgs.TagBase);
            _OldTagButton.TagBase = EventArgs.TagBase;
        }

        private void löschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_OldTagButton != null)
            {
                if (groupLinesTags.Items.Contains(_OldTagButton)) groupLinesTags.Items.Remove(_OldTagButton);
                if (groupLinesTargets.Items.Contains(_OldTagButton)) groupLinesTargets.Items.Remove(_OldTagButton);
                _Ui.DeleteTagBase(_OldTagButton.TagBase);
                _OldTagButton.Dispose();
            }
        }

        private void Bt_DropDown(object sender, ContextMenuArgs e)
        {
            _OldTagButton = (KrTagButton)sender;
        }
        
        private void btReset_Click(object sender, EventArgs e)
        {
            _Ui.Reset();
            ScrollDataGridView();
        }

        private void btClearList_Click(object sender, EventArgs e)
        {
            _Ui.Clear();
            pbMain.Image = null;
        }

        private void cmdCloseApplication_Execute(object sender, EventArgs e)
        {
            Application.Exit();
        }


        //uilogic event handling
        private void Ui_newPictureReady(object sender, EventArgs e)
        {
            _Reloading = true; //lock table for changes
            pbMain.Image = _Ui.CurrentImage;
            dgFiles.ClearSelection();
            ScrollDataGridView();
            _Reloading = false;
        }

        private void Ui_newTableReady(object sender, EventArgs e)
        {
            _Reloading = true;
            dgFiles.DataSource = _Ui.UiDataTable;
            for (int i = 0; i < dgFiles.Rows.Count; i++)
            {
                dgFiles.Rows[i].Cells[1].Style = _Ui.Styles[i, 0];
                dgFiles.Rows[i].Cells[2].Style = _Ui.Styles[i, 1];
                dgFiles.Rows[i].Cells[3].Style = _Ui.Styles[i, 2];
            }
            ScrollDataGridView();
            _Reloading = false;            
            ChangeStatusbar();
            FitToScreen();
        }

        private void ui_TagBaseNewEvent(object sender, TagBaseEventArgs Args)
        {
            KrTagButton bt = new KrTagButton(Args.TagBase);
            bt.TextLine1 = bt.TagBase.Name;
            bt.ButtonType = GroupButtonType.Split;
            bt.ContextMenuStrip = cmsUpdate;

            if (Args.TagBase.Color != Color.FromName("Control"))
            {
                Bitmap bmp = new Bitmap(16, 16);
                using (Graphics gfx = Graphics.FromImage(bmp))
                using (SolidBrush brush = new SolidBrush(Args.TagBase.Color))
                {
                    gfx.FillRectangle(brush, 0, 0, 16, 16);
                }
                bt.ImageLarge = bmp;
                bt.ImageSmall = bmp;

            }
            if (Args.TagBase.GetType() == typeof(Tag)) groupLinesTags.Items.Add(bt);
            else if (Args.TagBase.GetType() == typeof(Target)) groupLinesTargets.Items.Add(bt);
            else throw new InvalidCastException("Konnte Typ der TagBase nicht ermitteln!");
            bt.Click += tagBaseButtonClick;
            bt.DropDown += Bt_DropDown;


        }

        private void ui_TagBaseChanged(object sender, TagBaseEventArgs EventArgs)
        {
            try
            {
                _OldTagButton.TagBase = EventArgs.TagBase;
                _OldTagButton.TextLine1 = EventArgs.TagBase.Name;
                if (EventArgs.TagBase.Color != Color.FromName("Control"))
                {
                    Bitmap bmp = new Bitmap(16, 16);
                    using (Graphics gfx = Graphics.FromImage(bmp))
                    using (SolidBrush brush = new SolidBrush(EventArgs.TagBase.Color))
                    {
                        gfx.FillRectangle(brush, 0, 0, 16, 16);
                    }
                    _OldTagButton.ImageLarge = bmp;
                    _OldTagButton.ImageSmall = bmp;

                }
            }
            catch (NullReferenceException ex)
            {

                MessageBox.Show("Null Reference!" + ex.Message);
            }
        }

        private void Ui_TagBaseDeletedEvent(object sender, TagBaseEventArgs EventArgs)
        {
            TagBase tb = EventArgs.TagBase;
            if (tb.GetType() == typeof(Tag))
            {
                try
                {
                    foreach (KryptonRibbonGroupButton bt in groupLinesTags.Items)
                    {
                        KrTagButton btn = (KrTagButton)bt;
                        if (btn.TagBase == tb)
                        {
                            groupLinesTags.Items.Remove(btn);
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ShowError("Löschfehler: " + ex.Message);
                }
            }

            if (tb.GetType() == typeof(Target))
            {
                try
                {
                    foreach (KryptonRibbonGroupButton bt in groupLinesTargets.Items)
                    {
                        KrTagButton btn = (KrTagButton)bt;
                        if (btn.TagBase == tb)
                        {
                            groupLinesTargets.Items.Remove(btn);
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ShowError("Löschfehler: " + ex.Message);
                }
            }

        }

        private void MainForm_newTarget(object sender, EventArgs e)
        {
            var tnew = new TargetEditor(_Ui);
            tnew.Show();
            tnew.NewTarget += Tnew_NewTagBase;
        }

        private void MainForm_newTag(object sender, EventArgs e)
        {
            var tnew = new TagEditor(_Ui);
            tnew.Show();
            tnew.NewTag += Tnew_NewTagBase;
        }

        private void Tnew_NewTagBase(object sender, TagBaseEventArgs EventArgs)
        {
            _Ui.AddTagBase(EventArgs.TagBase);
        }

        private void _Ui_newProgress(object sender, EventArgs e)
        {
          if (this.InvokeRequired)
            {
                Invoke(new Action(SetProgress));  
            }
           
        }

        #endregion

        #region interface
        public string ShowFolderDialog()
        {
            OpenFolderDialog();
            return folderBrowser.SelectedPath;
        }

        public void ShowError(string ErrMsg)
        {
            MessageBox.Show(ErrMsg, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void newTagDialog()
        {
          
        }

        public void EditKeyBinds()
        {
            _KeyBindOverview.ShowDialog();
        }

        public void ShowMsg(string pMsg)
        {
            MessageBox.Show(pMsg, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ZoomIn()
        {
            btZoomIn_Click(null, null);
        }

        public void ZoomOut()
        {
            btZoomOut_Click(null, null);
        }

        void IPicSortUi.FitToScreen()
        {
            FitToScreen();
        }

        public void OrigSize()
        {
            btOriginalSize_Click(null, null);
        }

        public void ShowWarning(string WarnMsg)
        {
            MessageBox.Show(WarnMsg, "Warnung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public bool ShowYesNoDialog(string question)
        {
            var res = MessageBox.Show(question, "Frage an den Benutzer", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes) return true;
            else return false;
        }

        #endregion

        private void dgFiles_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!_Reloading) _Ui.SetFileIndex(e.RowIndex);
        }
    }
}
