﻿using ComponentFactory.Krypton.Ribbon;
using ComponentFactory.Krypton.Toolkit;
using System.Windows.Forms;
using PicSort.File;
using System.Collections.Generic;
using System.Linq;
using System;

namespace PicSortWinF
{
    internal static class Utilities
    {
        public static IEnumerable<T> AllControls<T>(this Control startingPoint) where T : Control
        {
            bool hit = startingPoint is T;
            if (hit)
            {
                yield return startingPoint as T;
            }
            foreach (var child in startingPoint.Controls.Cast<Control>())
            {
                foreach (var item in AllControls<T>(child))
                {
                    yield return item;
                }
            }
        }
    }

    internal class KrTagButton : KryptonRibbonGroupButton
    {
        public TagBase TagBase;

        public KrTagButton(TagBase tagBase) : base()
        {
            TagBase = tagBase;
        }
    }

    internal class KeyButton : KryptonButton
    {
        public Keys Key { get; set; }

        public KeyButton(): base()
        {

        }
    }
}