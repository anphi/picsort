﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;

using PicSort;
using PicSort.gui;

namespace PicSortWinF
{


    public partial class KeyBindOverview : KryptonForm
    {

        #region Felder
        #endregion

        #region Properties
        public UiLogic Logic { get; protected set; }
        #endregion

        #region Events
        #endregion

        #region Konstruktor
        public KeyBindOverview(UiLogic logic)
        {
            Logic = logic;
            InitializeComponent();
            ReloadOverview();
        }


        #endregion

        #region Destruktor
        #endregion

        #region Methoden
        public new DialogResult ShowDialog()
        {
            ReloadOverview();
            return base.ShowDialog();
        }
        #endregion

        #region Hilfsmethoden
        private void ReloadOverview()
        {
            var edit = this.AllControls<KeyButton>();
            Dictionary<Keys, KeyBindDescriptor> dic = Logic.ReturnKeyBinds();
            foreach (KeyButton bt in edit)
            {
                if (dic.ContainsKey(bt.Key)) bt.ButtonStyle = ButtonStyle.Custom2;
                else bt.ButtonStyle = ButtonStyle.Standalone;
            }
        }

        private void ExportOverview()
        {
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                Logic.ExportKeybinds(saveFile.FileName);
            }
        }

        #endregion

        #region Ereignismethoden
        #endregion

        private void KeyBtClick(object sender, EventArgs e)
        {
            KeyButton sd = (KeyButton)sender;
            var tmp = new KeyBindConf(sd, Logic);
            
            tmp.EditDone += Tmp_EditDone;
            tmp.ShowDialog();
        }

        private void Tmp_EditDone(object sender, EventArgs e)
        {
            ReloadOverview();
        }

        private void btExport_Click(object sender, EventArgs e)
        {
            ExportOverview();
        }
    }
}
