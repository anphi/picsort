﻿namespace PicSortWinF
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribMain = new ComponentFactory.Krypton.Ribbon.KryptonRibbon();
            this.OpenFolder = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem();
            this.cmdOpenFolder = new ComponentFactory.Krypton.Toolkit.KryptonCommand();
            this.Quit = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem();
            this.cmdCloseApplication = new ComponentFactory.Krypton.Toolkit.KryptonCommand();
            this.rtPic = new ComponentFactory.Krypton.Ribbon.KryptonRibbonTab();
            this.rgPhoto = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.kryptonRibbonGroupLines1 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupLines();
            this.btNext = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btPrevious = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupSeparator1 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator();
            this.kryptonRibbonGroupTriple4 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btUndo = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btRedo = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupSeparator2 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator();
            this.kryptonRibbonGroupTriple1 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btDelete = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btRotateLeft = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btRotateRight = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupTriple10 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btOverhead = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroup1 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.kryptonRibbonGroupTriple2 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btZoomIn = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btZoomOut = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btFitToScreen = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupTriple3 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btOriginalSize = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroup5 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.kryptonRibbonGroupTriple5 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btExec = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btReset = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btClearList = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonTab2 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonTab();
            this.gpTargets = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.tpTarget = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btNewTarget = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupSeparator3 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator();
            this.groupLinesTargets = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupLines();
            this.kryptonRibbonGroup2 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.kryptonRibbonGroupTriple7 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btNewTag = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupSeparator4 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator();
            this.groupLinesTags = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupLines();
            this.kryptonRibbonTab1 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonTab();
            this.grXML = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.kryptonRibbonGroupTriple6 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btSaveXML = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btLoadXml = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.btAddXmlItems = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.grKeybinds = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup();
            this.kryptonRibbonGroupTriple8 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.bteditKeybinds = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.kryptonRibbonGroupTriple9 = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple();
            this.btSettings = new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton();
            this.dgFiles = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.pnPicture = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.pbMain = new System.Windows.Forms.PictureBox();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.lblPictureCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDeletedPics = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUnsortedPics = new System.Windows.Forms.ToolStripStatusLabel();
            this.pgbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lblProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.kryptonContextMenuItems1 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItem1 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem();
            this.kryptonContextMenuItems3 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItems4 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItems5 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItems6 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItems7 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItems8 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.kryptonContextMenuItems9 = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.cmsUpdate = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ändernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.ribMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnPicture)).BeginInit();
            this.pnPicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMain)).BeginInit();
            this.statusBar.SuspendLayout();
            this.cmsUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribMain
            // 
            this.ribMain.Cursor = System.Windows.Forms.Cursors.Default;
            this.ribMain.ImeMode = System.Windows.Forms.ImeMode.Katakana;
            this.ribMain.InDesignHelperMode = true;
            this.ribMain.Name = "ribMain";
            this.ribMain.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2013White;
            this.ribMain.QATLocation = ComponentFactory.Krypton.Ribbon.QATLocation.Hidden;
            this.ribMain.QATUserChange = false;
            this.ribMain.RibbonAppButton.AppButtonMenuItems.AddRange(new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase[] {
            this.OpenFolder,
            this.Quit});
            this.ribMain.RibbonAppButton.AppButtonShowRecentDocs = false;
            this.ribMain.RibbonTabs.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonTab[] {
            this.rtPic,
            this.kryptonRibbonTab2,
            this.kryptonRibbonTab1});
            this.ribMain.SelectedTab = this.rtPic;
            this.ribMain.ShowMinimizeButton = false;
            this.ribMain.Size = new System.Drawing.Size(1640, 115);
            this.ribMain.TabIndex = 1;
            this.ribMain.TabStop = false;
            // 
            // OpenFolder
            // 
            this.OpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("OpenFolder.Image")));
            this.OpenFolder.KryptonCommand = this.cmdOpenFolder;
            this.OpenFolder.Text = "Ordner öffnen";
            // 
            // cmdOpenFolder
            // 
            this.cmdOpenFolder.Text = "OpenFolder";
            this.cmdOpenFolder.Execute += new System.EventHandler(this.cmdOpenFolder_Execute);
            // 
            // Quit
            // 
            this.Quit.Image = ((System.Drawing.Image)(resources.GetObject("Quit.Image")));
            this.Quit.KryptonCommand = this.cmdCloseApplication;
            this.Quit.Text = "Anwendung beenden";
            // 
            // cmdCloseApplication
            // 
            this.cmdCloseApplication.Text = "Anwendung schließen";
            this.cmdCloseApplication.Execute += new System.EventHandler(this.cmdCloseApplication_Execute);
            // 
            // rtPic
            // 
            this.rtPic.Groups.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup[] {
            this.rgPhoto,
            this.kryptonRibbonGroup1,
            this.kryptonRibbonGroup5});
            this.rtPic.Text = "Bild";
            // 
            // rgPhoto
            // 
            this.rgPhoto.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.kryptonRibbonGroupLines1,
            this.kryptonRibbonGroupSeparator1,
            this.kryptonRibbonGroupTriple4,
            this.kryptonRibbonGroupSeparator2,
            this.kryptonRibbonGroupTriple1,
            this.kryptonRibbonGroupTriple10});
            this.rgPhoto.TextLine1 = "Navigation";
            // 
            // kryptonRibbonGroupLines1
            // 
            this.kryptonRibbonGroupLines1.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btNext,
            this.btPrevious});
            // 
            // btNext
            // 
            this.btNext.ImageLarge = ((System.Drawing.Image)(resources.GetObject("btNext.ImageLarge")));
            this.btNext.ImageSmall = ((System.Drawing.Image)(resources.GetObject("btNext.ImageSmall")));
            this.btNext.KeyTip = "-";
            this.btNext.TextLine1 = "Nächstes";
            this.btNext.Click += new System.EventHandler(this.btNext_Click);
            // 
            // btPrevious
            // 
            this.btPrevious.ImageLarge = ((System.Drawing.Image)(resources.GetObject("btPrevious.ImageLarge")));
            this.btPrevious.ImageSmall = ((System.Drawing.Image)(resources.GetObject("btPrevious.ImageSmall")));
            this.btPrevious.TextLine1 = "Vorheriges";
            this.btPrevious.Click += new System.EventHandler(this.btPrevious_Click);
            // 
            // kryptonRibbonGroupTriple4
            // 
            this.kryptonRibbonGroupTriple4.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btUndo,
            this.btRedo});
            // 
            // btUndo
            // 
            this.btUndo.ImageLarge = global::PicSortWinF.Properties.Resources.undo;
            this.btUndo.TextLine1 = "Rückgängig";
            this.btUndo.Click += new System.EventHandler(this.btUndo_Click);
            // 
            // btRedo
            // 
            this.btRedo.ImageLarge = global::PicSortWinF.Properties.Resources.redo;
            this.btRedo.TextLine1 = "Wiederherstellen";
            this.btRedo.Click += new System.EventHandler(this.btRedo_Click);
            // 
            // kryptonRibbonGroupTriple1
            // 
            this.kryptonRibbonGroupTriple1.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btDelete,
            this.btRotateLeft,
            this.btRotateRight});
            // 
            // btDelete
            // 
            this.btDelete.ImageLarge = global::PicSortWinF.Properties.Resources.delete;
            this.btDelete.ImageSmall = ((System.Drawing.Image)(resources.GetObject("btDelete.ImageSmall")));
            this.btDelete.TextLine1 = "löschen";
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btRotateLeft
            // 
            this.btRotateLeft.ImageLarge = global::PicSortWinF.Properties.Resources.rotLeft;
            this.btRotateLeft.TextLine1 = "90° ";
            this.btRotateLeft.Click += new System.EventHandler(this.btRotateLeft_Click);
            // 
            // btRotateRight
            // 
            this.btRotateRight.ImageLarge = global::PicSortWinF.Properties.Resources.rotRight;
            this.btRotateRight.TextLine1 = "90°";
            this.btRotateRight.Click += new System.EventHandler(this.btRotateRight_Click);
            // 
            // kryptonRibbonGroupTriple10
            // 
            this.kryptonRibbonGroupTriple10.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btOverhead});
            // 
            // btOverhead
            // 
            this.btOverhead.ImageLarge = global::PicSortWinF.Properties.Resources.rotOH;
            this.btOverhead.TextLine1 = "180°";
            this.btOverhead.Click += new System.EventHandler(this.btOverhead_Click);
            // 
            // kryptonRibbonGroup1
            // 
            this.kryptonRibbonGroup1.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.kryptonRibbonGroupTriple2,
            this.kryptonRibbonGroupTriple3});
            this.kryptonRibbonGroup1.TextLine1 = "Ansicht";
            // 
            // kryptonRibbonGroupTriple2
            // 
            this.kryptonRibbonGroupTriple2.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btZoomIn,
            this.btZoomOut,
            this.btFitToScreen});
            // 
            // btZoomIn
            // 
            this.btZoomIn.ImageLarge = global::PicSortWinF.Properties.Resources.zoomIn;
            this.btZoomIn.TextLine1 = "Vergrößern";
            this.btZoomIn.Click += new System.EventHandler(this.btZoomIn_Click);
            // 
            // btZoomOut
            // 
            this.btZoomOut.ImageLarge = global::PicSortWinF.Properties.Resources.zoomOut;
            this.btZoomOut.TextLine1 = "Verkleinern";
            this.btZoomOut.Click += new System.EventHandler(this.btZoomOut_Click);
            // 
            // btFitToScreen
            // 
            this.btFitToScreen.ImageLarge = ((System.Drawing.Image)(resources.GetObject("btFitToScreen.ImageLarge")));
            this.btFitToScreen.TextLine1 = "FitToScreen";
            this.btFitToScreen.Click += new System.EventHandler(this.btFitToScreen_Click);
            // 
            // kryptonRibbonGroupTriple3
            // 
            this.kryptonRibbonGroupTriple3.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btOriginalSize});
            // 
            // btOriginalSize
            // 
            this.btOriginalSize.ImageLarge = ((System.Drawing.Image)(resources.GetObject("btOriginalSize.ImageLarge")));
            this.btOriginalSize.TextLine1 = "Originalgröße";
            this.btOriginalSize.Click += new System.EventHandler(this.btOriginalSize_Click);
            // 
            // kryptonRibbonGroup5
            // 
            this.kryptonRibbonGroup5.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.kryptonRibbonGroupTriple5});
            this.kryptonRibbonGroup5.TextLine1 = "Warteschlange";
            // 
            // kryptonRibbonGroupTriple5
            // 
            this.kryptonRibbonGroupTriple5.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btExec,
            this.btReset,
            this.btClearList});
            // 
            // btExec
            // 
            this.btExec.ImageLarge = global::PicSortWinF.Properties.Resources.exec;
            this.btExec.TextLine1 = "Warteschlange ";
            this.btExec.TextLine2 = "ausführen";
            this.btExec.Click += new System.EventHandler(this.btExec_Click);
            // 
            // btReset
            // 
            this.btReset.TextLine1 = "Änderungen verwerfen";
            this.btReset.TextLine2 = "(ohne Speichern)";
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // btClearList
            // 
            this.btClearList.TextLine1 = "Dateien schließen";
            this.btClearList.TextLine2 = "(ohne speichern)";
            this.btClearList.Click += new System.EventHandler(this.btClearList_Click);
            // 
            // kryptonRibbonTab2
            // 
            this.kryptonRibbonTab2.Groups.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup[] {
            this.gpTargets,
            this.kryptonRibbonGroup2});
            this.kryptonRibbonTab2.Text = "Markierungen";
            // 
            // gpTargets
            // 
            this.gpTargets.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.tpTarget,
            this.kryptonRibbonGroupSeparator3,
            this.groupLinesTargets});
            this.gpTargets.TextLine1 = "Ziele";
            // 
            // tpTarget
            // 
            this.tpTarget.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btNewTarget});
            // 
            // btNewTarget
            // 
            this.btNewTarget.ImageLarge = global::PicSortWinF.Properties.Resources.newTarget1;
            this.btNewTarget.TextLine1 = "Neues Ziel";
            this.btNewTarget.Click += new System.EventHandler(this.MainForm_newTarget);
            // 
            // kryptonRibbonGroup2
            // 
            this.kryptonRibbonGroup2.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.kryptonRibbonGroupTriple7,
            this.kryptonRibbonGroupSeparator4,
            this.groupLinesTags});
            this.kryptonRibbonGroup2.TextLine1 = "Tags";
            // 
            // kryptonRibbonGroupTriple7
            // 
            this.kryptonRibbonGroupTriple7.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btNewTag});
            // 
            // btNewTag
            // 
            this.btNewTag.ImageLarge = global::PicSortWinF.Properties.Resources.newTag1;
            this.btNewTag.TextLine1 = "Neues Tag";
            this.btNewTag.Click += new System.EventHandler(this.MainForm_newTag);
            // 
            // kryptonRibbonTab1
            // 
            this.kryptonRibbonTab1.Groups.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup[] {
            this.grXML,
            this.grKeybinds});
            this.kryptonRibbonTab1.Text = "profile";
            // 
            // grXML
            // 
            this.grXML.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.kryptonRibbonGroupTriple6});
            this.grXML.TextLine1 = "XML";
            // 
            // kryptonRibbonGroupTriple6
            // 
            this.kryptonRibbonGroupTriple6.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btSaveXML,
            this.btLoadXml,
            this.btAddXmlItems});
            // 
            // btSaveXML
            // 
            this.btSaveXML.ImageLarge = global::PicSortWinF.Properties.Resources.saveXML1;
            this.btSaveXML.TextLine1 = "Speichern";
            this.btSaveXML.Click += new System.EventHandler(this.btSaveXML_Click);
            // 
            // btLoadXml
            // 
            this.btLoadXml.ImageLarge = global::PicSortWinF.Properties.Resources.loadXML1;
            this.btLoadXml.TextLine1 = "Laden";
            this.btLoadXml.Click += new System.EventHandler(this.btLoadXml_Click);
            // 
            // btAddXmlItems
            // 
            this.btAddXmlItems.ImageLarge = global::PicSortWinF.Properties.Resources.loadXML1;
            this.btAddXmlItems.TextLine1 = "Tag/Targets";
            this.btAddXmlItems.TextLine2 = "hinzufügen";
            this.btAddXmlItems.Click += new System.EventHandler(this.btAddXmlItems_Click);
            // 
            // grKeybinds
            // 
            this.grKeybinds.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer[] {
            this.kryptonRibbonGroupTriple8,
            this.kryptonRibbonGroupTriple9});
            this.grKeybinds.TextLine1 = "Keybinds";
            // 
            // kryptonRibbonGroupTriple8
            // 
            this.kryptonRibbonGroupTriple8.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.bteditKeybinds});
            // 
            // bteditKeybinds
            // 
            this.bteditKeybinds.ImageLarge = global::PicSortWinF.Properties.Resources.keybinds;
            this.bteditKeybinds.TextLine1 = "Bearbeiten";
            this.bteditKeybinds.Click += new System.EventHandler(this.bteditKeybinds_Click);
            // 
            // kryptonRibbonGroupTriple9
            // 
            this.kryptonRibbonGroupTriple9.Items.AddRange(new ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem[] {
            this.btSettings});
            // 
            // btSettings
            // 
            this.btSettings.ImageLarge = global::PicSortWinF.Properties.Resources.settings;
            this.btSettings.TextLine1 = "Einstellungen";
            this.btSettings.Click += new System.EventHandler(this.btSettings_Click);
            // 
            // dgFiles
            // 
            this.dgFiles.AllowUserToAddRows = false;
            this.dgFiles.AllowUserToDeleteRows = false;
            this.dgFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgFiles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFiles.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgFiles.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.dgFiles.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ControlRibbonAppMenu;
            this.dgFiles.GridStyles.StyleColumn = ComponentFactory.Krypton.Toolkit.GridStyle.Sheet;
            this.dgFiles.GridStyles.StyleRow = ComponentFactory.Krypton.Toolkit.GridStyle.Sheet;
            this.dgFiles.Location = new System.Drawing.Point(1301, 145);
            this.dgFiles.Name = "dgFiles";
            this.dgFiles.ReadOnly = true;
            this.dgFiles.RowHeadersVisible = false;
            this.dgFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFiles.Size = new System.Drawing.Size(339, 518);
            this.dgFiles.TabIndex = 3;
            this.dgFiles.VirtualMode = true;
            this.dgFiles.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFiles_CellClick);
            // 
            // pnPicture
            // 
            this.pnPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnPicture.AutoScroll = true;
            this.pnPicture.Controls.Add(this.pbMain);
            this.pnPicture.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.pnPicture.Location = new System.Drawing.Point(1, 145);
            this.pnPicture.Name = "pnPicture";
            this.pnPicture.Size = new System.Drawing.Size(1294, 518);
            this.pnPicture.TabIndex = 4;
            // 
            // pbMain
            // 
            this.pbMain.BackColor = System.Drawing.Color.Black;
            this.pbMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbMain.ImageLocation = "";
            this.pbMain.Location = new System.Drawing.Point(0, 0);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(3872, 2592);
            this.pbMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMain.TabIndex = 1;
            this.pbMain.TabStop = false;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblPictureCount,
            this.lblDeletedPics,
            this.lblUnsortedPics,
            this.pgbProgress,
            this.lblProgress});
            this.statusBar.Location = new System.Drawing.Point(0, 666);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1640, 22);
            this.statusBar.TabIndex = 5;
            this.statusBar.Text = "stbProgressBar";
            // 
            // lblPictureCount
            // 
            this.lblPictureCount.Name = "lblPictureCount";
            this.lblPictureCount.Size = new System.Drawing.Size(108, 17);
            this.lblPictureCount.Text = "geladene Bilder: XX";
            // 
            // lblDeletedPics
            // 
            this.lblDeletedPics.Name = "lblDeletedPics";
            this.lblDeletedPics.Size = new System.Drawing.Size(161, 17);
            this.lblDeletedPics.Text = "zum Löschen vorgemerkt: XX";
            // 
            // lblUnsortedPics
            // 
            this.lblUnsortedPics.Name = "lblUnsortedPics";
            this.lblUnsortedPics.Size = new System.Drawing.Size(79, 17);
            this.lblUnsortedPics.Text = "Unsortiert: XX";
            // 
            // pgbProgress
            // 
            this.pgbProgress.Name = "pgbProgress";
            this.pgbProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // lblProgress
            // 
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(33, 17);
            this.lblProgress.Text = "xx: %";
            // 
            // kryptonContextMenuItems1
            // 
            this.kryptonContextMenuItems1.Items.AddRange(new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase[] {
            this.kryptonContextMenuItem1});
            // 
            // kryptonContextMenuItem1
            // 
            this.kryptonContextMenuItem1.Text = "Menu Item";
            // 
            // cmsUpdate
            // 
            this.cmsUpdate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cmsUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ändernToolStripMenuItem,
            this.löschenToolStripMenuItem});
            this.cmsUpdate.Name = "contextMenuStrip1";
            this.cmsUpdate.Size = new System.Drawing.Size(119, 48);
            // 
            // ändernToolStripMenuItem
            // 
            this.ändernToolStripMenuItem.Name = "ändernToolStripMenuItem";
            this.ändernToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.ändernToolStripMenuItem.Text = "Ändern";
            this.ändernToolStripMenuItem.Click += new System.EventHandler(this.ändernToolStripMenuItem_Click);
            // 
            // löschenToolStripMenuItem
            // 
            this.löschenToolStripMenuItem.Name = "löschenToolStripMenuItem";
            this.löschenToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.löschenToolStripMenuItem.Text = "Löschen";
            this.löschenToolStripMenuItem.Click += new System.EventHandler(this.löschenToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1640, 688);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.pnPicture);
            this.Controls.Add(this.dgFiles);
            this.Controls.Add(this.ribMain);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "PicSort";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ribMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnPicture)).EndInit();
            this.pnPicture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMain)).EndInit();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.cmsUpdate.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ComponentFactory.Krypton.Ribbon.KryptonRibbon ribMain;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonTab rtPic;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonTab kryptonRibbonTab2;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupLines kryptonRibbonGroupLines1;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btNext;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btPrevious;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple1;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btDelete;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btRotateLeft;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btRotateRight;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator kryptonRibbonGroupSeparator1;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem OpenFolder;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem Quit;
        public ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup kryptonRibbonGroup1;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple2;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btZoomIn;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btZoomOut;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btFitToScreen;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple3;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btOriginalSize;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnPicture;
        public ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup rgPhoto;
        private System.Windows.Forms.PictureBox pbMain;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel lblPictureCount;
        private System.Windows.Forms.ToolStripStatusLabel lblDeletedPics;
        private System.Windows.Forms.ToolStripStatusLabel lblUnsortedPics;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonTab kryptonRibbonTab1;
        private ComponentFactory.Krypton.Toolkit.KryptonCommand cmdOpenFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        public ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgFiles;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple4;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btUndo;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btRedo;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator kryptonRibbonGroupSeparator2;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup gpTargets;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple tpTarget;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btNewTarget;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup kryptonRibbonGroup2;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup kryptonRibbonGroup5;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple5;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btExec;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator kryptonRibbonGroupSeparator3;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupLines groupLinesTargets;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple7;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btNewTag;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupLines groupLinesTags;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupSeparator kryptonRibbonGroupSeparator4;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems1;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem kryptonContextMenuItem1;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems3;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems4;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems5;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems6;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems7;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems8;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems kryptonContextMenuItems9;
        private System.Windows.Forms.ContextMenuStrip cmsUpdate;
        private System.Windows.Forms.ToolStripMenuItem ändernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup grXML;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple6;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btSaveXML;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btLoadXml;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup grKeybinds;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple8;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton bteditKeybinds;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple9;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btSettings;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple kryptonRibbonGroupTriple10;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btOverhead;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btReset;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btClearList;
        private ComponentFactory.Krypton.Toolkit.KryptonCommand cmdCloseApplication;
        private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btAddXmlItems;
        private System.Windows.Forms.ToolStripProgressBar pgbProgress;
        private System.Windows.Forms.ToolStripStatusLabel lblProgress;
        //   private ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton btNewTag;
    }
}

