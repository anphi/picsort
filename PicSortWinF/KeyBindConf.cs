﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFactory.Krypton.Toolkit;
using System.Windows.Forms;
using System.Drawing;

using PicSort;
using PicSort.gui;
using PicSort.File;

namespace PicSortWinF
{
    internal partial class KeyBindConf : KryptonForm
    {
        #region fields
        private UiLogic _Logic;
        private KeyButton _Parent;
        private Keys _Key;
        #endregion

        #region properties

        /* this is empty */

        #endregion

        #region events
        public event EventHandler EditDone;
        #endregion

        #region constructor

        public KeyBindConf(KeyButton sender, UiLogic logic)
        {
            _Logic = logic;
            _Parent = sender;
            _Key = sender.Key;
            InitializeComponent();
            LoadEntries();
            LookUpBind();
            CheckedChange(this, new EventArgs());
        }

        #endregion

        #region destruktor

        /* this is empty */

        #endregion

        #region methods

        /* this is empty */

        #endregion

        #region helpingMethods

        private void LoadEntries()
        {
            Array val = Enum.GetValues(typeof(GuiActions));
            foreach (GuiActions entry in val)
            {
                cmbGuiAction.Items.Add(entry.ToString());
            }
            foreach (TagBase t in _Logic.TagList)
            {
                if (t.GetType() == typeof(Tag)) cmbAssignment.Items.Add(t.Name);
            }
            cmbAssignment.Items.Add("--------");
            foreach (TagBase tr in _Logic.TagList)
            {
                if (tr.GetType() == typeof(Target)) cmbAssignment.Items.Add(tr.Name);
            }

            try
            {
                lblButton.Text = _Parent.Text;

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void LookUpBind()
        {
            try
            {
                KeyBindDescriptor desc;
                Dictionary<Keys, KeyBindDescriptor> dic = _Logic.ReturnKeyBinds();
                if (dic.TryGetValue(_Key, out desc))
                {
                    if (desc.ActionType == GuiActions.TagTarget)
                    {
                        rbGuiAction.Checked = false;
                        rbAssignment.Checked = true;
                        cmbAssignment.SelectedItem = desc.KeyBindTag.Name;
                    }
                    else
                    {
                        rbGuiAction.Checked = true;
                        rbAssignment.Checked = false;
                        cmbGuiAction.SelectedItem = desc.ActionType.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler:" + ex.Message);
            }
        }

        #endregion

        #region eventCallBacks

        private void btClear_Click(object sender, EventArgs e)
        {
            _Logic.KeyBindDelete(_Key);
            EditDone?.Invoke(this, new EventArgs());
            this.Close();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            if (rbAssignment.Checked)
            {
                TagBase tmp = _Logic.ReturnTagBase(cmbAssignment.SelectedItem.ToString());
                _Logic.KeyBindUpdate(
                        _Parent.Key,
                        new KeyBindDescriptor(GuiActions.TagTarget, () => _Logic.AssignTagBase((Tag)tmp), tmp));

                this.Close();

            }
            if (rbGuiAction.Checked)
            {
                GuiActions action = (GuiActions)Enum.Parse(typeof(GuiActions), cmbGuiAction.SelectedItem.ToString(), true);
                _Logic.KeyBindUpdate(_Parent.Key, new KeyBindDescriptor(action, _Logic.ReturnDefaultActionDelegate(action)));
            }
            EditDone?.Invoke(this, new EventArgs());
            this.Dispose();
        }
        #endregion

        #region interface

        /* this is empty */

        #endregion

        #region Designer
        private KryptonLabel kryptonLabel1;
        private KryptonLabel lblButton;
        private KryptonButton btClear;
        private KryptonButton btSave;
        private KryptonComboBox cmbAssignment;
        private KryptonRadioButton rbGuiAction;
        private KryptonRadioButton rbAssignment;
        private KryptonComboBox cmbGuiAction;
        private System.ComponentModel.IContainer components;



        private void InitializeComponent()
        {
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblButton = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btClear = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btSave = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cmbAssignment = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.rbGuiAction = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.rbAssignment = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.cmbGuiAction = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAssignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGuiAction)).BeginInit();
            this.SuspendLayout();
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(12, 21);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(50, 20);
            this.kryptonLabel1.TabIndex = 1;
            this.kryptonLabel1.Values.Text = "Button:";
            // 
            // lblButton
            // 
            this.lblButton.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.BoldPanel;
            this.lblButton.Location = new System.Drawing.Point(77, 21);
            this.lblButton.Name = "lblButton";
            this.lblButton.Size = new System.Drawing.Size(94, 20);
            this.lblButton.TabIndex = 2;
            this.lblButton.Values.Text = "kryptonLabel2";
            // 
            // btClear
            // 
            this.btClear.Location = new System.Drawing.Point(27, 146);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(106, 37);
            this.btClear.TabIndex = 3;
            this.btClear.Values.Text = "Löschen";
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(157, 146);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(106, 37);
            this.btSave.TabIndex = 5;
            this.btSave.Values.Text = "Speichern";
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // cmbAssignment
            // 
            this.cmbAssignment.DropDownWidth = 121;
            this.cmbAssignment.Location = new System.Drawing.Point(157, 106);
            this.cmbAssignment.Name = "cmbAssignment";
            this.cmbAssignment.Size = new System.Drawing.Size(121, 21);
            this.cmbAssignment.TabIndex = 7;
            // 
            // rbGuiAction
            // 
            this.rbGuiAction.Location = new System.Drawing.Point(12, 80);
            this.rbGuiAction.Name = "rbGuiAction";
            this.rbGuiAction.Size = new System.Drawing.Size(82, 20);
            this.rbGuiAction.TabIndex = 8;
            this.rbGuiAction.Values.Text = "GUI Aktion";
            this.rbGuiAction.CheckedChanged += new System.EventHandler(this.CheckedChange);
            // 
            // rbAssignment
            // 
            this.rbAssignment.Location = new System.Drawing.Point(157, 80);
            this.rbAssignment.Name = "rbAssignment";
            this.rbAssignment.Size = new System.Drawing.Size(81, 20);
            this.rbAssignment.TabIndex = 9;
            this.rbAssignment.Values.Text = "Zuweisung";
            this.rbAssignment.CheckedChanged += new System.EventHandler(this.CheckedChange);
            // 
            // cmbGuiAction
            // 
            this.cmbGuiAction.DropDownWidth = 121;
            this.cmbGuiAction.Location = new System.Drawing.Point(12, 106);
            this.cmbGuiAction.Name = "cmbGuiAction";
            this.cmbGuiAction.Size = new System.Drawing.Size(121, 21);
            this.cmbGuiAction.TabIndex = 6;
            // 
            // KeyBindConf
            // 
            this.ClientSize = new System.Drawing.Size(300, 196);
            this.Controls.Add(this.rbAssignment);
            this.Controls.Add(this.rbGuiAction);
            this.Controls.Add(this.cmbAssignment);
            this.Controls.Add(this.cmbGuiAction);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btClear);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.kryptonLabel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "KeyBindConf";
            this.Text = "Button neu belegen";
            ((System.ComponentModel.ISupportInitialize)(this.cmbAssignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGuiAction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void CheckedChange(object sender, EventArgs e)
        {
            cmbAssignment.Enabled = rbAssignment.Checked;
            cmbGuiAction.Enabled = rbGuiAction.Checked;
        }

        #endregion

    }
}
