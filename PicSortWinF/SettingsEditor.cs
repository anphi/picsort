﻿using System;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
using PicSort;

namespace PicSortWinF
{
    public partial class SettingsEditor : KryptonForm
    {
        #region Felder
        UiLogic _Ui;
        #endregion

        #region Properties
        #endregion

        #region Events
        #endregion

        #region Konstruktor
        public SettingsEditor(UiLogic ui)
        {
            InitializeComponent();
            _Ui = ui;
            InitControls();
            ActivateControls();
            
        }

        #endregion

        #region Destruktor
        #endregion

        #region Methoden

        #endregion

        #region Hilfsmethoden
        private void InitControls()
        {
            if (_Ui.SStartupImportXml)
            {
                cbLoadCfgOnStart.Checked = true;
                tbCfgPath.Text = _Ui.SPathStartUpXml;
            }
            else cbLoadCfgOnStart.Checked = false;

            if (_Ui.SStartupImportFolder)
            {
                cbFolderImportOnStart.Checked = true;
                tbFolderPath.Text = _Ui.SPathStartUpFolder;
             
            }
            else cbFolderImportOnStart.Checked = false;

            cbNextFileOnAssignment.Checked =_Ui.SSkipOnAssignment;
            cbNextFileOnDel.Checked = _Ui.SSkipOnDelete;
            tbTimeStampTag.Text = _Ui.STimeStampTag;
            tbTimeStampTarget.Text = _Ui.STimeStampTarget;
            numLeadingZeroes.Value = _Ui.SLeadingZeros;
            cbTrashcan.Checked = _Ui.SMoveFileToTrashcan;
            numScrollCount.Value = _Ui.SScrollCount;
        }

        private void ActivateControls()
        {
            tbCfgPath.Enabled = cbLoadCfgOnStart.Checked;
            tbFolderPath.Enabled = cbFolderImportOnStart.Checked;

            btFolderOpenFileDialog.Enabled = cbFolderImportOnStart.Checked;
            btConfigFileOpenDialog.Enabled = cbLoadCfgOnStart.Checked;
        }

        private void WriteSettings()
        {
            _Ui.SLeadingZeros = (int) numLeadingZeroes.Value;
            _Ui.STimeStampTag = tbTimeStampTag.Text;
            _Ui.STimeStampTarget = tbTimeStampTarget.Text;
            _Ui.SMoveFileToTrashcan = cbTrashcan.Checked;
            _Ui.SScrollCount = (int) numScrollCount.Value;
        }
        
        #endregion

        #region Ereignismethoden
        private void cbLoadCfgOnStart_CheckedChanged(object sender, EventArgs e)
        {
            _Ui.SStartupImportXml = cbLoadCfgOnStart.Checked;
            ActivateControls();
        }

        private void cbFolderImportOnStart_CheckedChanged(object sender, EventArgs e)
        {
            _Ui.SStartupImportFolder = cbFolderImportOnStart.Checked;
            ActivateControls();
        }

        private void tbCfgPath_Click(object sender, EventArgs e)
        {
            tbCfgPath.SelectAll();
        }

        private void btConfigFileOpenDialog_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = _Ui.SPathLastXml;
            openFileDialog.Filter = "XML Dateien | *.xml";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _Ui.SStartupImportXml = true;
                _Ui.SPathStartUpXml = openFileDialog.FileName;
                _Ui.SPathLastXml = openFileDialog.FileName;
                tbCfgPath.Text = openFileDialog.FileName;
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            WriteSettings();
            this.Dispose();
        }

        private void btFolderOpenFileDialog_Click(object sender, EventArgs e)
        {
            openFolderDialog.SelectedPath = _Ui.SPathLastFolder;
            if (openFolderDialog.ShowDialog() == DialogResult.OK)
            {
                _Ui.SStartupImportFolder = true;
                _Ui.SPathStartUpFolder = openFolderDialog.SelectedPath;
                tbFolderPath.Text = openFolderDialog.SelectedPath;
            }

        }

        private void cbNextFileOnAssignment_CheckedChanged(object sender, EventArgs e)
        {
           _Ui.SSkipOnAssignment = cbNextFileOnAssignment.Checked;
            InitControls();
        }

        private void cbNextFileOnDel_CheckedChanged(object sender, EventArgs e)
        {
            _Ui.SSkipOnDelete = cbNextFileOnDel.Checked;
            InitControls();
        }

        private void kryptonLinkLabel1_LinkClicked(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://docs.microsoft.com/de-de/dotnet/standard/base-types/custom-date-and-time-format-strings");
        }
        #endregion
        
    }
}
