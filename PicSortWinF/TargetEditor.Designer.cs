﻿namespace PicSortWinF
{
    partial class TargetEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btBrowse = new System.Windows.Forms.Button();
            this.gbRename = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCustomString = new System.Windows.Forms.TextBox();
            this.rbCustom = new System.Windows.Forms.RadioButton();
            this.rbCounter = new System.Windows.Forms.RadioButton();
            this.rbTimestamp = new System.Windows.Forms.RadioButton();
            this.tbKuerzel = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.btChooseColor = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.cbEnableRenameTool = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.gbRename.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(12, 77);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(293, 20);
            this.tbPath.TabIndex = 2;
            this.tbPath.Text = "<Pfad eingeben>";
            this.tbPath.Click += new System.EventHandler(this.tbPath_Click);
            // 
            // btBrowse
            // 
            this.btBrowse.Location = new System.Drawing.Point(310, 76);
            this.btBrowse.Name = "btBrowse";
            this.btBrowse.Size = new System.Drawing.Size(97, 20);
            this.btBrowse.TabIndex = 3;
            this.btBrowse.Text = "Durchsuchen";
            this.btBrowse.UseVisualStyleBackColor = true;
            this.btBrowse.Click += new System.EventHandler(this.btBrowse_Click);
            // 
            // gbRename
            // 
            this.gbRename.Controls.Add(this.label1);
            this.gbRename.Controls.Add(this.tbCustomString);
            this.gbRename.Controls.Add(this.rbCustom);
            this.gbRename.Controls.Add(this.rbCounter);
            this.gbRename.Controls.Add(this.rbTimestamp);
            this.gbRename.Location = new System.Drawing.Point(12, 128);
            this.gbRename.Name = "gbRename";
            this.gbRename.Size = new System.Drawing.Size(142, 166);
            this.gbRename.TabIndex = 2;
            this.gbRename.TabStop = false;
            this.gbRename.Text = "Umbenennen";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 45);
            this.label1.TabIndex = 5;
            this.label1.Text = "Eigene Beschriftung wird nach dem Text fortlaufend nummeriert";
            // 
            // tbCustomString
            // 
            this.tbCustomString.Location = new System.Drawing.Point(25, 86);
            this.tbCustomString.Name = "tbCustomString";
            this.tbCustomString.Size = new System.Drawing.Size(101, 20);
            this.tbCustomString.TabIndex = 8;
            // 
            // rbCustom
            // 
            this.rbCustom.AutoSize = true;
            this.rbCustom.Location = new System.Drawing.Point(6, 63);
            this.rbCustom.Name = "rbCustom";
            this.rbCustom.Size = new System.Drawing.Size(120, 17);
            this.rbCustom.TabIndex = 7;
            this.rbCustom.TabStop = true;
            this.rbCustom.Text = "Eigene Beschriftung";
            this.rbCustom.UseVisualStyleBackColor = true;
            this.rbCustom.CheckedChanged += new System.EventHandler(this.rbCustom_CheckedChanged);
            // 
            // rbCounter
            // 
            this.rbCounter.AutoSize = true;
            this.rbCounter.Location = new System.Drawing.Point(6, 40);
            this.rbCounter.Name = "rbCounter";
            this.rbCounter.Size = new System.Drawing.Size(108, 17);
            this.rbCounter.TabIndex = 6;
            this.rbCounter.TabStop = true;
            this.rbCounter.Text = "laufende Nummer";
            this.rbCounter.UseVisualStyleBackColor = true;
            // 
            // rbTimestamp
            // 
            this.rbTimestamp.AutoSize = true;
            this.rbTimestamp.Location = new System.Drawing.Point(6, 17);
            this.rbTimestamp.Name = "rbTimestamp";
            this.rbTimestamp.Size = new System.Drawing.Size(82, 17);
            this.rbTimestamp.TabIndex = 5;
            this.rbTimestamp.TabStop = true;
            this.rbTimestamp.Text = "Erstelldatum";
            this.rbTimestamp.UseVisualStyleBackColor = true;
            // 
            // tbKuerzel
            // 
            this.tbKuerzel.Location = new System.Drawing.Point(12, 51);
            this.tbKuerzel.Name = "tbKuerzel";
            this.tbKuerzel.Size = new System.Drawing.Size(395, 20);
            this.tbKuerzel.TabIndex = 1;
            this.tbKuerzel.Text = "<Kürzel eingeben>";
            this.tbKuerzel.Click += new System.EventHandler(this.tbKuerzel_Click);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(12, 25);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(395, 20);
            this.tbName.TabIndex = 0;
            this.tbName.Text = "<Name eingeben>";
            this.tbName.Click += new System.EventHandler(this.tbName_Click);
            // 
            // btChooseColor
            // 
            this.btChooseColor.Location = new System.Drawing.Point(160, 105);
            this.btChooseColor.Name = "btChooseColor";
            this.btChooseColor.Size = new System.Drawing.Size(247, 46);
            this.btChooseColor.TabIndex = 9;
            this.btChooseColor.Text = "Farbe wählen";
            this.btChooseColor.UseVisualStyleBackColor = true;
            this.btChooseColor.Click += new System.EventHandler(this.btChooseColor_Click);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(230, 260);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(111, 34);
            this.btSave.TabIndex = 10;
            this.btSave.Text = "Speichern und schließen";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // cbEnableRenameTool
            // 
            this.cbEnableRenameTool.AutoSize = true;
            this.cbEnableRenameTool.Location = new System.Drawing.Point(12, 105);
            this.cbEnableRenameTool.Name = "cbEnableRenameTool";
            this.cbEnableRenameTool.Size = new System.Drawing.Size(119, 17);
            this.cbEnableRenameTool.TabIndex = 4;
            this.cbEnableRenameTool.Text = "Umbenenner aktiv?";
            this.cbEnableRenameTool.UseVisualStyleBackColor = true;
            this.cbEnableRenameTool.CheckedChanged += new System.EventHandler(this.cbEnableRenameTool_CheckedChanged);
            // 
            // newTarget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 302);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btChooseColor);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.tbKuerzel);
            this.Controls.Add(this.gbRename);
            this.Controls.Add(this.cbEnableRenameTool);
            this.Controls.Add(this.btBrowse);
            this.Controls.Add(this.tbPath);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "newTarget";
            this.Text = "neues Ziel erstellen";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.newTarget_KeyDown);
            this.gbRename.ResumeLayout(false);
            this.gbRename.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btBrowse;
        private System.Windows.Forms.GroupBox gbRename;
        private System.Windows.Forms.RadioButton rbCustom;
        private System.Windows.Forms.RadioButton rbCounter;
        private System.Windows.Forms.RadioButton rbTimestamp;
        private System.Windows.Forms.CheckBox cbEnableRenameTool;
        private System.Windows.Forms.TextBox tbKuerzel;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button btChooseColor;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCustomString;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}