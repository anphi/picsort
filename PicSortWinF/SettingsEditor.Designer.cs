﻿namespace PicSortWinF
{
    partial class SettingsEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbStart = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btFolderOpenFileDialog = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tbFolderPath = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cbFolderImportOnStart = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.btConfigFileOpenDialog = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tbCfgPath = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cbLoadCfgOnStart = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btClose = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.openFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.cbNextFileOnAssignment = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.cbNextFileOnDel = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.tbTimeStampTag = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbTimeStampTarget = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.numLeadingZeroes = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblHelpLink = new ComponentFactory.Krypton.Toolkit.KryptonLinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTrashcan = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.numScrollCount = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.gbStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbStart.Panel)).BeginInit();
            this.gbStart.Panel.SuspendLayout();
            this.gbStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbStart
            // 
            this.gbStart.CaptionOverlap = 0D;
            this.gbStart.CaptionStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.BoldControl;
            this.gbStart.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.gbStart.Location = new System.Drawing.Point(12, 12);
            this.gbStart.Name = "gbStart";
            // 
            // gbStart.Panel
            // 
            this.gbStart.Panel.Controls.Add(this.btFolderOpenFileDialog);
            this.gbStart.Panel.Controls.Add(this.tbFolderPath);
            this.gbStart.Panel.Controls.Add(this.cbFolderImportOnStart);
            this.gbStart.Panel.Controls.Add(this.btConfigFileOpenDialog);
            this.gbStart.Panel.Controls.Add(this.tbCfgPath);
            this.gbStart.Panel.Controls.Add(this.cbLoadCfgOnStart);
            this.gbStart.Size = new System.Drawing.Size(281, 161);
            this.gbStart.TabIndex = 0;
            this.gbStart.Values.Heading = "Start";
            // 
            // btFolderOpenFileDialog
            // 
            this.btFolderOpenFileDialog.Location = new System.Drawing.Point(204, 101);
            this.btFolderOpenFileDialog.Name = "btFolderOpenFileDialog";
            this.btFolderOpenFileDialog.Size = new System.Drawing.Size(50, 25);
            this.btFolderOpenFileDialog.TabIndex = 7;
            this.btFolderOpenFileDialog.Values.Text = "...";
            this.btFolderOpenFileDialog.Click += new System.EventHandler(this.btFolderOpenFileDialog_Click);
            // 
            // tbFolderPath
            // 
            this.tbFolderPath.Location = new System.Drawing.Point(3, 101);
            this.tbFolderPath.Name = "tbFolderPath";
            this.tbFolderPath.Size = new System.Drawing.Size(195, 23);
            this.tbFolderPath.TabIndex = 6;
            this.tbFolderPath.Text = "<Pfad>";
            // 
            // cbFolderImportOnStart
            // 
            this.cbFolderImportOnStart.Location = new System.Drawing.Point(3, 75);
            this.cbFolderImportOnStart.Name = "cbFolderImportOnStart";
            this.cbFolderImportOnStart.Size = new System.Drawing.Size(195, 20);
            this.cbFolderImportOnStart.TabIndex = 5;
            this.cbFolderImportOnStart.Values.Text = "Ordner beim Start importieren?";
            this.cbFolderImportOnStart.CheckedChanged += new System.EventHandler(this.cbFolderImportOnStart_CheckedChanged);
            // 
            // btConfigFileOpenDialog
            // 
            this.btConfigFileOpenDialog.Location = new System.Drawing.Point(204, 35);
            this.btConfigFileOpenDialog.Name = "btConfigFileOpenDialog";
            this.btConfigFileOpenDialog.Size = new System.Drawing.Size(50, 25);
            this.btConfigFileOpenDialog.TabIndex = 4;
            this.btConfigFileOpenDialog.Values.Text = "...";
            this.btConfigFileOpenDialog.Click += new System.EventHandler(this.btConfigFileOpenDialog_Click);
            // 
            // tbCfgPath
            // 
            this.tbCfgPath.Location = new System.Drawing.Point(3, 35);
            this.tbCfgPath.Name = "tbCfgPath";
            this.tbCfgPath.Size = new System.Drawing.Size(195, 23);
            this.tbCfgPath.TabIndex = 1;
            this.tbCfgPath.Text = "<Pfad>";
            this.tbCfgPath.Click += new System.EventHandler(this.tbCfgPath_Click);
            // 
            // cbLoadCfgOnStart
            // 
            this.cbLoadCfgOnStart.Location = new System.Drawing.Point(3, 9);
            this.cbLoadCfgOnStart.Name = "cbLoadCfgOnStart";
            this.cbLoadCfgOnStart.Size = new System.Drawing.Size(199, 20);
            this.cbLoadCfgOnStart.TabIndex = 0;
            this.cbLoadCfgOnStart.Values.Text = "Konfiguration beim Start Laden?";
            this.cbLoadCfgOnStart.CheckedChanged += new System.EventHandler(this.cbLoadCfgOnStart_CheckedChanged);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(371, 473);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(90, 25);
            this.btClose.TabIndex = 2;
            this.btClose.Values.Text = "Schließen";
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // cbNextFileOnAssignment
            // 
            this.cbNextFileOnAssignment.Location = new System.Drawing.Point(319, 12);
            this.cbNextFileOnAssignment.Name = "cbNextFileOnAssignment";
            this.cbNextFileOnAssignment.Size = new System.Drawing.Size(319, 20);
            this.cbNextFileOnAssignment.TabIndex = 3;
            this.cbNextFileOnAssignment.Values.Text = "Bei Target Zuweisung automatisch nächstes Bild laden";
            this.cbNextFileOnAssignment.CheckedChanged += new System.EventHandler(this.cbNextFileOnAssignment_CheckedChanged);
            // 
            // cbNextFileOnDel
            // 
            this.cbNextFileOnDel.Location = new System.Drawing.Point(319, 38);
            this.cbNextFileOnDel.Name = "cbNextFileOnDel";
            this.cbNextFileOnDel.Size = new System.Drawing.Size(210, 20);
            this.cbNextFileOnDel.TabIndex = 4;
            this.cbNextFileOnDel.Values.Text = "Nach Löschen nächste Datei laden";
            this.cbNextFileOnDel.CheckedChanged += new System.EventHandler(this.cbNextFileOnDel_CheckedChanged);
            // 
            // tbTimeStampTag
            // 
            this.tbTimeStampTag.AcceptsReturn = true;
            this.tbTimeStampTag.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.tbTimeStampTag.Location = new System.Drawing.Point(12, 189);
            this.tbTimeStampTag.Name = "tbTimeStampTag";
            this.tbTimeStampTag.Size = new System.Drawing.Size(116, 23);
            this.tbTimeStampTag.TabIndex = 5;
            this.tbTimeStampTag.Text = "Timestamp Tag";
            // 
            // tbTimeStampTarget
            // 
            this.tbTimeStampTarget.Location = new System.Drawing.Point(12, 224);
            this.tbTimeStampTarget.Name = "tbTimeStampTarget";
            this.tbTimeStampTarget.Size = new System.Drawing.Size(116, 23);
            this.tbTimeStampTarget.TabIndex = 6;
            this.tbTimeStampTarget.Text = "Timestamp Target";
            // 
            // numLeadingZeroes
            // 
            this.numLeadingZeroes.Location = new System.Drawing.Point(12, 256);
            this.numLeadingZeroes.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numLeadingZeroes.Name = "numLeadingZeroes";
            this.numLeadingZeroes.Size = new System.Drawing.Size(116, 22);
            this.numLeadingZeroes.TabIndex = 7;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(145, 192);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(160, 20);
            this.kryptonLabel1.TabIndex = 8;
            this.kryptonLabel1.Values.Text = "Timestamp Format für Tags";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(145, 224);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(175, 20);
            this.kryptonLabel2.TabIndex = 9;
            this.kryptonLabel2.Values.Text = "Timestamp Format für Targets";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(145, 258);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(211, 20);
            this.kryptonLabel3.TabIndex = 10;
            this.kryptonLabel3.Values.Text = "Anzahl an Stellen bei Nummerierung";
            // 
            // lblHelpLink
            // 
            this.lblHelpLink.Location = new System.Drawing.Point(357, 207);
            this.lblHelpLink.Name = "lblHelpLink";
            this.lblHelpLink.Size = new System.Drawing.Size(36, 20);
            this.lblHelpLink.TabIndex = 11;
            this.lblHelpLink.Values.Text = "Hilfe";
            this.lblHelpLink.LinkClicked += new System.EventHandler(this.kryptonLinkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(311, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 59);
            this.label1.TabIndex = 12;
            this.label1.Text = "}";
            // 
            // cbTrashcan
            // 
            this.cbTrashcan.Location = new System.Drawing.Point(319, 64);
            this.cbTrashcan.Name = "cbTrashcan";
            this.cbTrashcan.Size = new System.Drawing.Size(233, 20);
            this.cbTrashcan.TabIndex = 13;
            this.cbTrashcan.Values.Text = "Datei in Papierkorbordner verschieben";
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(145, 286);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(308, 20);
            this.kryptonLabel4.TabIndex = 15;
            this.kryptonLabel4.Values.Text = "Anzahl der oberen Zellen beim automatischen Scrollen";
            // 
            // numScrollCount
            // 
            this.numScrollCount.Location = new System.Drawing.Point(12, 284);
            this.numScrollCount.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numScrollCount.Name = "numScrollCount";
            this.numScrollCount.Size = new System.Drawing.Size(116, 22);
            this.numScrollCount.TabIndex = 14;
            // 
            // SettingsEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 554);
            this.Controls.Add(this.kryptonLabel4);
            this.Controls.Add(this.numScrollCount);
            this.Controls.Add(this.cbTrashcan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblHelpLink);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.numLeadingZeroes);
            this.Controls.Add(this.tbTimeStampTarget);
            this.Controls.Add(this.tbTimeStampTag);
            this.Controls.Add(this.cbNextFileOnDel);
            this.Controls.Add(this.cbNextFileOnAssignment);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.gbStart);
            this.Name = "SettingsEditor";
            this.Text = "SettingsEditor";
            ((System.ComponentModel.ISupportInitialize)(this.gbStart.Panel)).EndInit();
            this.gbStart.Panel.ResumeLayout(false);
            this.gbStart.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbStart)).EndInit();
            this.gbStart.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox gbStart;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btFolderOpenFileDialog;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbFolderPath;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox cbFolderImportOnStart;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btConfigFileOpenDialog;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbCfgPath;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox cbLoadCfgOnStart;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btClose;
        private System.Windows.Forms.FolderBrowserDialog openFolderDialog;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox cbNextFileOnAssignment;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox cbNextFileOnDel;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbTimeStampTag;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbTimeStampTarget;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown numLeadingZeroes;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLinkLabel lblHelpLink;
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox cbTrashcan;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown numScrollCount;
    }
}