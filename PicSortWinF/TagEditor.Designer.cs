﻿namespace PicSortWinF
{
    partial class TagEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSave = new System.Windows.Forms.Button();
            this.btChooseColor = new System.Windows.Forms.Button();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbKuerzel = new System.Windows.Forms.TextBox();
            this.gbRename = new System.Windows.Forms.GroupBox();
            this.tbCustomString = new System.Windows.Forms.TextBox();
            this.rbCustom = new System.Windows.Forms.RadioButton();
            this.rbTimestamp = new System.Windows.Forms.RadioButton();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.rbPrefix = new System.Windows.Forms.RadioButton();
            this.rbSuffix = new System.Windows.Forms.RadioButton();
            this.gbRename.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(160, 147);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(100, 41);
            this.btSave.TabIndex = 6;
            this.btSave.Text = "Speichern und schließen";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btChooseColor
            // 
            this.btChooseColor.Location = new System.Drawing.Point(160, 95);
            this.btChooseColor.Name = "btChooseColor";
            this.btChooseColor.Size = new System.Drawing.Size(100, 46);
            this.btChooseColor.TabIndex = 5;
            this.btChooseColor.Text = "Farbe wählen";
            this.btChooseColor.UseVisualStyleBackColor = true;
            this.btChooseColor.Click += new System.EventHandler(this.btChooseColor_Click);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(12, 12);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(248, 20);
            this.tbName.TabIndex = 0;
            this.tbName.Text = "<Name eingeben>";
            this.tbName.Click += new System.EventHandler(this.tbName_Click);
            // 
            // tbKuerzel
            // 
            this.tbKuerzel.Location = new System.Drawing.Point(12, 38);
            this.tbKuerzel.Name = "tbKuerzel";
            this.tbKuerzel.Size = new System.Drawing.Size(248, 20);
            this.tbKuerzel.TabIndex = 1;
            this.tbKuerzel.Text = "<Kürzel eingeben>";
            this.tbKuerzel.Click += new System.EventHandler(this.tbKuerzel_Click);
            // 
            // gbRename
            // 
            this.gbRename.Controls.Add(this.tbCustomString);
            this.gbRename.Controls.Add(this.rbCustom);
            this.gbRename.Controls.Add(this.rbTimestamp);
            this.gbRename.Location = new System.Drawing.Point(12, 95);
            this.gbRename.Name = "gbRename";
            this.gbRename.Size = new System.Drawing.Size(142, 93);
            this.gbRename.TabIndex = 10;
            this.gbRename.TabStop = false;
            this.gbRename.Text = "Umbenennen";
            // 
            // tbCustomString
            // 
            this.tbCustomString.Location = new System.Drawing.Point(25, 64);
            this.tbCustomString.Name = "tbCustomString";
            this.tbCustomString.Size = new System.Drawing.Size(101, 20);
            this.tbCustomString.TabIndex = 4;
            // 
            // rbCustom
            // 
            this.rbCustom.AutoSize = true;
            this.rbCustom.Location = new System.Drawing.Point(6, 41);
            this.rbCustom.Name = "rbCustom";
            this.rbCustom.Size = new System.Drawing.Size(120, 17);
            this.rbCustom.TabIndex = 3;
            this.rbCustom.TabStop = true;
            this.rbCustom.Text = "Eigene Beschriftung";
            this.rbCustom.UseVisualStyleBackColor = true;
            this.rbCustom.CheckedChanged += new System.EventHandler(this.rbCustom_CheckedChanged);
            // 
            // rbTimestamp
            // 
            this.rbTimestamp.AutoSize = true;
            this.rbTimestamp.Location = new System.Drawing.Point(6, 18);
            this.rbTimestamp.Name = "rbTimestamp";
            this.rbTimestamp.Size = new System.Drawing.Size(82, 17);
            this.rbTimestamp.TabIndex = 2;
            this.rbTimestamp.TabStop = true;
            this.rbTimestamp.Text = "Erstelldatum";
            this.rbTimestamp.UseVisualStyleBackColor = true;
            // 
            // rbPrefix
            // 
            this.rbPrefix.AutoSize = true;
            this.rbPrefix.Location = new System.Drawing.Point(15, 72);
            this.rbPrefix.Name = "rbPrefix";
            this.rbPrefix.Size = new System.Drawing.Size(51, 17);
            this.rbPrefix.TabIndex = 11;
            this.rbPrefix.TabStop = true;
            this.rbPrefix.Text = "Prefix";
            this.rbPrefix.UseVisualStyleBackColor = true;
            // 
            // rbSuffix
            // 
            this.rbSuffix.AutoSize = true;
            this.rbSuffix.Location = new System.Drawing.Point(87, 72);
            this.rbSuffix.Name = "rbSuffix";
            this.rbSuffix.Size = new System.Drawing.Size(51, 17);
            this.rbSuffix.TabIndex = 12;
            this.rbSuffix.TabStop = true;
            this.rbSuffix.Text = "Suffix";
            this.rbSuffix.UseVisualStyleBackColor = true;
            // 
            // TagEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 221);
            this.Controls.Add(this.rbSuffix);
            this.Controls.Add(this.rbPrefix);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btChooseColor);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.tbKuerzel);
            this.Controls.Add(this.gbRename);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagEditor";
            this.Text = "Neues Tag erstellen";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.newTag_KeyDown);
            this.gbRename.ResumeLayout(false);
            this.gbRename.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btChooseColor;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbKuerzel;
        private System.Windows.Forms.GroupBox gbRename;
        private System.Windows.Forms.TextBox tbCustomString;
        private System.Windows.Forms.RadioButton rbCustom;
        private System.Windows.Forms.RadioButton rbTimestamp;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.RadioButton rbPrefix;
        private System.Windows.Forms.RadioButton rbSuffix;
    }
}