using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace PicSortGTK
{
    class MainWindow : Window
    {
        #region gui
        
        [UI] private Label _label1 = null;
        [UI] private Button _button1 = null;

        #endregion

        #region Felder
        
        private int _counter;
        
        #endregion

        #region Properties

        #endregion

        #region Events
        
        #endregion

        #region Konstruktor
        
        public MainWindow() : this(new Builder("MainWindow.glade")) { }
        
        private MainWindow(Builder builder) : base(builder.GetObject("MainWindow").Handle)
        {
            builder.Autoconnect(this);

            DeleteEvent += Window_DeleteEvent;
            _button1.Clicked += Button1_Clicked;
        }

        #endregion

        #region Destruktor
        #endregion

        #region Methoden
        #endregion

        #region Hilfsmethoden
        #endregion

        #region Ereignismethoden
        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

        private void Button1_Clicked(object sender, EventArgs a)
        {
            _counter++;
            _label1.Text = "Hello World! This button has been clicked " + _counter + " time(s).";
        }
        #endregion



        

        

        
    }
}
