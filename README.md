# PicSort

### Was ist Picsort?  
PicSort wurde geschrieben, um möglichst effizient viele Bilder in verschiedene Ordner
einzusortieren. Der Benutzer kann verschiedene Zielordner und Tags für Dateien 
erstellen und diese bei Bedarf direkt auf Tasten legen, um sie schnell an Dateien zuzuweisen.

### Features
* Aufbau einer Warteschlange, anstatt Änderungen direkt zu schreiben
* Frei anpassbare Tastenkürzel, um schnell auf alle wichtigen Funktionen zuzugreifen
* Farbige Hervorhebung von Dateien
* Benutzung von XML Dateien, um Einstellungen und Konfigurationen zu speichern
* Einfache Orientierung im Programm durch die Ribbon Bar

### Installation
~~Unter [releases](https://github.com/anp369/picsort/releases) finden sich alle herausgebrachten Versionen  
Einfach die .zip Datei herunterladen und den Anweisungen der readme.txt folgen  
Zusätzlich enthält der release branch die aktuelle benutzbare version.~~

### Deinstallation
Zur Deinstallation einfach den Anweisungen in der readme.txt folgen, die in jedem Download vorhanden ist

### Bugs
Sollten irgendwelche Fehler oder Unstimmigkeiten mit dem Programm auftauchen, bitte hier in GitLab einen [Issue](https://gitlab.com/anphi/picsort/-/issues) eröffnen.  
Sofern ich gerade Zeit habe, versuche ich mich zeitnah darum zu kümmern. Auch Vorschläge für weitere Features sind erwünscht!
