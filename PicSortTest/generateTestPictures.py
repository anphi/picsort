#! /usr/bin/python3

import sys
import os
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from random import choice

HEIGHT = 768
WIDTH = 1024
FONTSIZE = 32

tags = [
        "pic of the day",
        "special",
        "tag1",
        "tag2",
        None,
        ]

targets = [
        "folder1",
        "folder2",
        "folder3",
        "folder4",
        ]
path = ""

def generatePictures(n: int, folder: str):
    for i in range(n):
        img = Image.new('RGB',(WIDTH,HEIGHT))
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("/usr/share/fonts/truetype/ubuntu/Ubuntu-B.ttf",FONTSIZE)
        #generate string
        gen = ""
        tag = choice(tags)
        if tag is not None: gen += tag 
        gen += '\n'
        gen += choice(targets)

        # draw rectangle
        draw.rectangle(((0,0),(WIDTH,HEIGHT)),fill="black")
        draw.text((500,300),gen,font=font)

        img.save(folder+str(i)+'.jpg','JPEG')
    
if __name__ == "__main__":
    try:
        path = sys.argv[1]
        count = int(sys.argv[2])
        if path[-1] != '/' or not os.path.exists(path):
            raise IOError()
        generatePictures(count,path)
    except IOError as ex:
        raise
        print("IO error: %s" % ex.strerror)

    except:
        raise
        print("Error while parsing the path")
