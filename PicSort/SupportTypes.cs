﻿using System;

namespace PicSort
{
    public enum Orientation { normal, left, right, head };
    public enum Status { unsorted, sorted, deleted };

    public enum TagPosition { prefix, suffix };
    public enum TagType { timestamp, customString, counter }

    public enum TargetType { noRename, timestamp, counter, customString }

    public enum Os { win,linux}

    public enum GuiActions
    {
        openFolder,
        openFile,
        delete,
        rotateLeft,
        rotateRight,
        rotateOverHead,
        nextFile,
        previousFile,
        executeQueue,
        TagTarget,
        undo,
        redo,
        zoomIn,
        zoomOut,
        fitToScreen,
        originalSize
    }

    public class TagBaseEventArgs : EventArgs
    {
        public File.TagBase TagBase;

        public TagBaseEventArgs(File.TagBase tagTarget) : base()
        {
            TagBase = tagTarget;
        }
    }
    
    public delegate void TagBaseEventHandler(object sender, TagBaseEventArgs EventArgs);

    public delegate void KeyStrokeAction();

}