﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PicSort.File
{
    /// <summary>
    /// represents a collection of all loaded files
    /// </summary>
    internal class FileList : List<FileRep>
    {
        #region Felder
        private int _UnsortedPics;

        /// <summary>
        /// Zeichen zur Trennung der Verzeichnisse
        /// Windows: \ 
        /// Unix: /
        /// </summary>
        private char _PathChar;

        /// <summary>
        /// einprogrammierte Liste mit erlaubten Dateiendungen 
        /// </summary>
        private List<string> _Extensions;

        #endregion

        #region Properties

        /// <summary>
        /// point to the currently selected file
        /// </summary>
        public FileRep ActiveFile
        {
            get
            {
                if (Count > 0) return this[FileIndex]; //zeige nur etwas an wenn Dateien da sind
                else return null;
            }
        }

        /// <summary>
        /// Index Pointer to the index of the currently selected file
        /// </summary>
        public int FileIndex { get; protected set; }

        /// <summary>
        /// Tells the caller how many of the files of an import process are imported
        /// </summary>
        public int Progress { get; protected set; }

        #endregion

        #region Events

        /// <summary>
            /// is raised, when all commited changes are executed
            /// </summary>
        public event EventHandler ListExecutionFinished;

        /// <summary>
        /// is raised when all files of a folder are fully loaded into the program
        /// </summary>
        public event EventHandler LoadFolderFinished;

        /// <summary>
        /// is raised when a different file from the current one is selected
        /// </summary>
        public event EventHandler NewFileSelected;

        public event EventHandler NewProgress;

        #endregion

        #region Konstruktor

        /// <summary>
        /// Constructor of the FileList
        /// </summary>
        /// <param name="pathChar">Char that is used to seperate between folders (Windows: \  Unix: /)</param>
        public FileList(char pathChar)
        {
            _PathChar = pathChar;
            FileIndex = 0;
            _UnsortedPics = 0;
            //HACK harcoded Extensions
            _Extensions = new List<string>();
            _Extensions.Add(".jpg");
            _Extensions.Add(".JPG");
            _Extensions.Add(".jpeg");
            _Extensions.Add(".JPEG");
            _Extensions.Add(".png");
            _Extensions.Add(".PNG");
            _Extensions.Add(".bmp");
        }

        #endregion

        #region Destruktor
        #endregion

        #region Methoden
        
        /// <summary>
        /// Clears the list and resets the FileIndex
        /// </summary>
        public new void Clear()
        {
            base.Clear();
            FileIndex = 0;
        }

        /// <summary>
        /// Loads a File into the list
        /// </summary>
        /// <param name="path">Path of the gile</param>
        public void LoadFile(string path)
        {
            this.Add(new FileRep(path));
            FileIndex = Count - 1; //Set index to the last element
        }

        /// <summary>
        /// Load an entire Folder into the list
        /// Doesn't work recursively
        /// </summary>
        /// <param name="path">path of the folder</param>
        public void LoadFolder(string path)
        {
            try
            {
                int FileCount = 0;
                List<string> Files = new List<string>();
                foreach (string f in Directory.GetFiles(path))
                {
                    if (_Extensions.Contains(f.Substring(f.LastIndexOf('.'))))
                    {
                        FileCount++;
                        Files.Add(f);
                    }
                }
                foreach (string f in Files)
                {
                    LoadFile(f);
                    Progress = (int)Math.Round((decimal)Count / FileCount * 100);
                    NewProgress?.Invoke(this, new EventArgs());
                }
                FileIndex = Count - 1;
                LoadFolderFinished?.Invoke(this, new EventArgs());
            }
            catch (IOException)
            {
                throw; //Error Handling wird mittels Try catch in einer Klasse weiter oben durchgeführt              
            }
        }

        /// <summary>
        /// shows the next file (index + 1) and presents it on ActiveFile
        /// </summary>
        public void NextFile()
        {
            if (FileIndex < Count - 1) FileIndex++;
            NewFileSelected?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// shows the previous file (index - 1) and presents it on Active File
        /// </summary>
        public void PreviousFile()
        {
            if (FileIndex > 0) FileIndex--;
            NewFileSelected?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Select an index to be presented
        /// Indices out of range are ignored
        /// </summary>
        /// <param name="index">Index</param>
        public void SetFileIndex(int index)
        {
            if (index >= 0 && index <= Count - 1) FileIndex = index;
            NewFileSelected?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// executes all staged operations in the queue
        /// </summary>
        public void Execute()
        {
            int FileCount = 0;
            CountUnsortedPics();
            foreach (FileRep f in this)
            {
                f.Execute();
                Progress = FileCount / (Count - _UnsortedPics);
                NewProgress?.Invoke(this, new EventArgs());
                FileIndex++;
            }
            ListExecutionFinished?.Invoke(this, new EventArgs());
        }

        #endregion

        #region Hilfsmethoden
        private void CountUnsortedPics()
        {
            foreach (FileRep File in this)
            {
                if (!File.NeedsExecution) _UnsortedPics++;
            }
        }
        #endregion

        #region Ereignismethoden
        #endregion
    }
}
