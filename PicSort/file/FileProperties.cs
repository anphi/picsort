﻿namespace PicSort.File
{
    /// <summary>
    /// stores properties of a file relevant for undo/redo
    /// </summary>
    internal class FileProperties
    {
        #region Felder

        /// <summary>
        /// the FileProperties Object to derive the data from
        /// </summary>
        private FileProperties _Parent;

        /// <summary>
        /// Zeichen zur Trennung der Verzeichnisse
        /// Windows: \ 
        /// Unix: /
        /// </summary>
        protected char _PathChar;

        #endregion

        #region Properties

        /// <summary>
        /// the current name of the File
        /// </summary>
        public string FileName
        {
            get { return Path.Substring(Path.LastIndexOf(_PathChar) + 1); }
            set
            {
                Path = Path.Substring(0, Path.LastIndexOf(_PathChar) + 1) + value;
            }
        }

        /// <summary>
        /// the current path of the File
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// the current orientation of the picture
        /// </summary>
        public Orientation Orientation { get; set; }

        /// <summary>
        /// the current state if the picture should be deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// the tag currently assigned to the file
        /// </summary>
        public Tag Tag { get; set; }

        /// <summary>
        /// the target currently assigned to the file
        /// </summary>
        public Target Target { get; set; }

        #endregion

        #region Events
        /* this is empty */
        #endregion

        #region Konstruktor

        /// <summary>
        /// constructor of the FileProperties Class
        /// use this for all calls except the first one of one file
        /// </summary>
        /// <param name="parent">the parent object to derive the date from</param>
        /// <param name="pathChar">char to seperate the directories in path strings</param>
        public FileProperties(FileProperties parent, char pathChar = '\\')
        {
            _Parent = parent;
            Path = parent.Path;
            Orientation = parent.Orientation;
            Deleted = parent.Deleted;
            Tag = parent.Tag;
            Target = parent.Target;
            _PathChar = pathChar;
        }

        /// <summary>
        /// constructor of the FileProperties Class
        /// use this for the first FileProperty to enter
        /// set information manually. It builds the first member of the chain
        /// </summary>
        /// <param name="path">path of the file</param>
        /// <param name="orientation">orientation of the file</param>
        /// <param name="del">flag if the file should be deleted</param>
        /// <param name="tag">tag the file holds currently</param>
        /// <param name="target">target the file holds currently</param>
        /// <param name="pathChar">char to seperate the directories in path strings</param>
        public FileProperties(string path, Orientation orientation, bool del, Tag tag, Target target, char pathChar = '\\')
        {
            Path = path;
            Orientation = orientation;
            Deleted = del;
            _Parent = null;
            Tag = tag;
            Target = target;
            _PathChar = pathChar;
        }

        #endregion

        #region Destruktor
        /* this is empty */
        #endregion

        #region Methoden
        /* this is empty */
        #endregion

        #region Hilfsmethoden
        /* this is empty */
        #endregion

        #region Ereignismethoden
        /* this is empty */
        #endregion
    }
}
