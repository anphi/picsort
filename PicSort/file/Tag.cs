﻿using System;
using System.Drawing;


namespace PicSort.File
{
    /// <summary>
    /// inherits from TagBase
    /// class representing a tag which is assigned to files
    /// </summary>
    public class Tag: TagBase
    {
        #region fields

        /* this is empty */

        #endregion

        #region properties

        /// <summary>
        /// counter for numbering
        /// </summary>
        public new static int Counter { get; protected set; }

        /// <summary>
        /// specifies where the tag will be written
        /// </summary>
        public TagPosition TagPosition { get; protected set; }

        /// <summary>
        /// specifies the type of the tag
        /// </summary>
        public TagType TagType { get; protected set; }

        #endregion

        #region events

        /* this is empty */


        #endregion

        #region constructor

        /// <summary>
        /// Constructor of the Tag class
        /// </summary>
        /// <param name="name"></param>
        /// <param name="token"></param>
        /// <param name="custonString"></param>
        /// <param name="color"></param>
        /// <param name="position"></param>
        /// <param name="tagType"></param>
        public Tag(string name, string token = "", string custonString = "", Color color = default(Color),
          TagPosition position = TagPosition.prefix, TagType tagType = TagType.customString) : base(name, token, custonString, color)
        {
            TagPosition = position;
            TagType = tagType;

            if (TagType == TagType.customString) CustomString = custonString;
            if (token == "") Token = base.Name.Substring(0, 4);
            else Token = token;
            CustomString = custonString;
        }

        #endregion

        #region destruktor

        /* this is empty */


        #endregion

        #region methods

        /// <summary>
        /// returns the string that should be added to the fileName
        /// </summary>
        /// <param name="time">optional: time that should appear int the fileName</param>
        /// <returns>the calulated string to insert</returns>
        public override string ReturnString(string name = "",DateTime time = default(DateTime))
        {
            switch (TagType)
            {    //Counter so überarbeiten, dass keine Überschreibungen zwischen verschiedenen Tags auftauchen, unwahrscheinlich, da 2 CounterTags erforderlich
                case TagType.counter:
                    string tmp = '[' + Counter.ToString() + ']';
                    Counter++;
                    return tmp;
                case TagType.customString:
                    return '[' + CustomString + ']';
                case TagType.timestamp:
                    return '[' + time.ToString(Settings.Default.timeFormatTag) + ']';
                default:
                    return '[' + "ERR" + ']';
            }

        }

        #endregion

        #region helpingMethods

        /* this is empty */


        #endregion

        #region eventCallBacks

        /* this is empty */


        #endregion

        #region interface

        /* this is empty */

        #endregion
    }
}
