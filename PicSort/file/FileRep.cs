﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Windows.Forms;


namespace PicSort.File
{
    /// <summary>
    /// represents a file with all relevant properties for this program
    /// stored properties:
    ///    versioned:
    ///      *filename + path
    ///      *orientation
    ///      *delete flag
    ///      *tag
    ///      *target
    ///     
    ///    unversioned:
    ///     *lastChanged date
    ///     *height
    ///     *width
    ///     *flags for processing
    /// </summary>
    internal class FileRep: INotifyPropertyChanged
    {
        #region fields

        /// <summary>
        /// char to seperate the directories in path strings
        /// Windows: \ 
        /// Unix: /
        /// </summary>
        private char _PathChar;

        /// <summary>
        /// List that holds all the versioned properties of a file
        /// </summary>
        private List<FileProperties> _Properties;

        /// <summary>
        /// index pointer that points on the currently selected
        /// version of properties. 
        /// </summary>
        /// <remarks>
        /// In most cases this is the latest
        /// bverion except some undo actions occured
        ///</remarks>
        private int _PropertyIndex;
        
        #endregion

        #region properties

        /// <summary>
        /// current Name of the file
        /// </summary>
        public string FileName
        {
            get { return _Properties[_PropertyIndex].FileName; }
            set
            {
                FileProperties tmp = new FileProperties(_Properties[_PropertyIndex]);
                tmp.FileName = value;
                if (_Properties.Count == _PropertyIndex + 1)
                {
                    _Properties.Add(tmp);
                    _PropertyIndex++;
                }
                else
                {
                    _Properties[_PropertyIndex + 1] = tmp;
                    _PropertyIndex++;
                }

                OnPropertyChanged("filename");
            }
        }        

        /// <summary>
        /// Height of the image
        /// </summary>
        public int Height { get; protected set; }

        /// <summary>
        /// flag: shows if the picture is queued for deletion
        /// </summary>
        public bool IsDeleted { get { return _Properties[_PropertyIndex].Deleted; } }

        /// <summary>
        /// flag: shows if all queued changes have been executed
        /// </summary>
        public bool IsExecuted { get; protected set; }

        /// <summary>
        /// stores the date of the last change
        /// extracted from the file system info
        /// </summary>
        public DateTime LastChange { get; protected set; }

        /// <summary>
        /// flag: shwows if changes were made and changed must be executed
        /// </summary>
        public bool NeedsExecution { get; protected set; }

        /// <summary>
        /// current orientation of the picture
        /// </summary>
        public Orientation Orientation { get { return _Properties[_PropertyIndex].Orientation; } }

        /// <summary>
        /// current path of the file including the filename
        /// </summary>
        public string Path
        {
            get { return _Properties.Last().Path; }
            set
            {
                FileProperties tmp = new FileProperties(_Properties[_PropertyIndex]);
                tmp.Path = value;
                if (_Properties.Count == _PropertyIndex + 1)
                {
                    _Properties.Add(tmp);
                    _PropertyIndex++;
                }
                else
                {
                    _Properties[_PropertyIndex + 1] = tmp;
                    _PropertyIndex++;
                }

                OnPropertyChanged("path");
            }
        }

        /// <summary>
        /// Tag currently hold by the file
        /// </summary>
        public Tag Tag { get { return _Properties[_PropertyIndex].Tag; } }

        /// <summary>
        /// Target currently hold by the file
        /// </summary>
        public Target Target { get { return _Properties[_PropertyIndex].Target; } }

        /// <summary>
        /// Width of the image
        /// </summary>
        public int Width { get; protected set; }

        #endregion

        #region events

        /// <summary>
        /// raised when all changes were written
        /// </summary>
        public event EventHandler ExecutionDone;

        /// <summary>
        /// raised when a property changes
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region constructors

        /// <summary>
        /// Constructor of a file representant
        /// </summary>
        /// <param name="path">path of the file</param>
        /// <param name="pathChar">char to seperate the directories in path strings</param>
        public FileRep(string path, char pathChar = '\\')
        {
            _PathChar = pathChar;
            _Properties = new List<FileProperties>();
            _PropertyIndex = 0;
            var tmp = new FileProperties(path, Orientation.normal, false, null, null);
            _Properties.Add(tmp);
            NeedsExecution = false;
            LastChange = System.IO.File.GetLastWriteTime(Path);
            using (Image img = Image.FromFile(path)) //this could be threaded for improved performance
            {
                Width = img.Width;
                Height = img.Height;
                img.Dispose();
            }
        }

        #endregion

        #region destructors

        /* this is empty */

        #endregion

        #region methods

        /// <summary>
        /// assigns the given tag to a file
        /// </summary>
        /// <param name="pTag">tag to assign</param>
        public void AssignTag(Tag pTag)
        {
            NeedsExecution = true;
            var tmp = new FileProperties(_Properties[_PropertyIndex]);
            tmp.Tag = pTag;
            if (_Properties.Count == _PropertyIndex + 1)
            {
                _Properties.Add(tmp);
                _PropertyIndex++;
            }
            else
            {
                _Properties[_PropertyIndex + 1] = tmp;
                _PropertyIndex++;
            }
            OnPropertyChanged("tag");
        }

        /// <summary>
        /// Assigns the given Target to the File
        /// </summary>
        /// <param name="target">Target to assign</param>
        public void AssignTarget(Target target)
        {
            NeedsExecution = true;
            var tmp = new FileProperties(_Properties[_PropertyIndex]);
            tmp.Target = target;
            //check if property gets appended or written in the middle
            if (_Properties.Count == _PropertyIndex + 1)
            {
                _Properties.Add(tmp);
                _PropertyIndex++;
            }
            else
            {
                _Properties[_PropertyIndex + 1] = tmp;
                _PropertyIndex++;
            }


            OnPropertyChanged("target");
        }             

        /// <summary>
        /// marks a file for Deletion
        /// </summary>
        public void Delete()
        {
            NeedsExecution = true;
            var tmp = new FileProperties(_Properties[_PropertyIndex]);
            tmp.Deleted = true;
            if (_Properties.Count == _PropertyIndex + 1)
            {
                _Properties.Add(tmp);
                _PropertyIndex++;
            }
            else
            {
                _Properties[_PropertyIndex + 1] = tmp;
                _PropertyIndex++;
            }
            OnPropertyChanged("deleted");
        }

        /// <summary>
        /// rotates a picture into the given direction
        /// </summary>
        /// <param name="pOrientation">orientation to rotate</param>
        public void Rotate(Orientation pOrientation)
        {
            NeedsExecution = true;
            var tmp = new FileProperties(_Properties[_PropertyIndex]);
            tmp.Orientation = pOrientation;
            if (_Properties.Count == _PropertyIndex + 1)
            {
                _Properties.Add(tmp);
                _PropertyIndex++;
            }
            else
            {
                _Properties[_PropertyIndex + 1] = tmp;
                _PropertyIndex++;
            }
            OnPropertyChanged("rotation");
        }

        /// <summary>
        /// restores the next version of properties
        /// </summary>
        public void Redo()
        {
            if (_PropertyIndex < _Properties.Count - 1) _PropertyIndex++;
            else throw new IndexOutOfRangeException("Es gibt nicht mehr Elemente");
            OnPropertyChanged("redo");
        }

        /// <summary>
        /// changes the name of a file
        /// </summary>
        /// <param name="name">new name</param>
        public void Rename(string name)
        {
            NeedsExecution = true;
            var tmp = new FileProperties(_Properties[_PropertyIndex]);
            tmp.Path = tmp.Path.Substring(0, tmp.Path.LastIndexOf(_PathChar) + 2 + 1); //cut file name with last pathChar
            tmp.Path += name;
            if (_Properties.Count == _PropertyIndex + 1)
            {
                _Properties.Add(tmp);
                _PropertyIndex++;
            }
            else
            {
                _Properties[_PropertyIndex + 1] = tmp;
                _PropertyIndex++;
            }
            OnPropertyChanged("filename");
        }

        /// <summary>
        /// restores the previous state of versioned properties
        /// </summary>
        public void Undo()
        {
            if (_PropertyIndex == 0) NeedsExecution = false;
            if (_PropertyIndex > 0) _PropertyIndex--;
            else throw new IndexOutOfRangeException("Index ist bereits 0");
            OnPropertyChanged("undo");
        }        


        /// <summary>
        /// executes all staged changes
        /// </summary>
        internal void Execute()
        {
            if (!IsExecuted && NeedsExecution)
            {
                try
                {
                    int actions = 0;
                    actions = actions + ProcessTarget();
                    actions = actions + ProcessTag();

                    if (_Properties[_PropertyIndex].Deleted)
                    {
                        if (Settings.Default.actMoveFileToTrash)
                        {
                            //Trash
                            string dir = System.IO.Path.GetDirectoryName(_Properties[0].Path) + _PathChar + ".trash" + _PathChar;
                            if (!Directory.Exists(dir)) { Directory.CreateDirectory(dir); }
                            System.IO.File.Move(_Properties[_PropertyIndex].Path, dir + FileName);
                        }
                        else
                        {
                            //delete
                            System.IO.File.Delete(_Properties[_PropertyIndex].Path);
                            actions++;
                        }

                    }
                    else
                    {
                        if (_Properties[_PropertyIndex].Path != _Properties[0].Path) { System.IO.File.Move(_Properties[0].Path, _Properties[_PropertyIndex].Path); actions++; } //move
                        if (_Properties[_PropertyIndex].Orientation != _Properties[0].Orientation) { ProcessRotation(_Properties[_PropertyIndex].Orientation); actions++; } //rotate
                    }
                    //if (actions == 0) throw new Exception("Keine Veränderung möglich");
                    ExecutionDone?.Invoke(this, new EventArgs());
                }
                catch (IOException ex)
                {

                    MessageBox.Show("IO EXCEPTION!: " + ex.Message);
                }
            }

        }

        /// <summary>
        /// resets the file to the original state
        /// </summary>
        internal void Reset()
        {
            _PropertyIndex = 0;
            OnPropertyChanged("reset");
        }

        #endregion

        #region helpingMethods

        /// <summary>
        /// processes the currently selected tag
        /// </summary>
        /// <returns>returns the amount of changes (0 or 1)</returns>
        private int ProcessTag()
        {
            if (Tag == null) return 0;
            string oldName = FileName;
            if (Tag.TagPosition == TagPosition.prefix)
            {
                FileName = Tag.ReturnString("",LastChange) + oldName;
                return 1;
            }
            else //suffix
            {
                string oldname = FileName;
                string ext = oldname.Substring(oldname.LastIndexOf('.')); //filter out extension
                FileName = oldname.Substring(0, oldname.LastIndexOf('.')) + Tag.ReturnString("",LastChange) + ext; //build new FileName
                return 1;
            }
        }

        /// <summary>
        /// processes the currently selected target
        /// </summary>
        /// <returns>returns amount of performed changes (0 or 1)</returns>
        private int ProcessTarget()
        {
            if (Target == null) return 0;
            Path = Target.ReturnString(FileName, LastChange);
            return 1;
        }

        /// <summary>
        /// rotates the image to the given rotation
        /// </summary>
        /// <param name="pOrientation">orientation to rotate</param>
        private void ProcessRotation(Orientation pOrientation)
        {

            //https://www.codeproject.com/articles/4861/an-image-viewer-with-lossless-rotation-exif-and-ot
            //ready up objects and Encoders
            Image img;
            string fileNameTmp;
            Encoder enc = Encoder.Transformation;
            EncoderParameters encPars = new EncoderParameters(1);
            EncoderParameter encPar = null;

            //load image
            ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
            img = Image.FromFile(Path);
            fileNameTmp = Path + ".temp";

            //set up Encoder
            if (pOrientation == Orientation.normal) throw new ArgumentException("Invalid Rotation");
            if (pOrientation == Orientation.right) encPar = new EncoderParameter(enc, (long)EncoderValue.TransformRotate90);
            if (pOrientation == Orientation.head) encPar = new EncoderParameter(enc, (long)EncoderValue.TransformRotate180);
            if (pOrientation == Orientation.left) encPar = new EncoderParameter(enc, (long)EncoderValue.TransformRotate270);
            encPars.Param[0] = encPar;

            //apply encoder to image
            img.Save(fileNameTmp, codecInfo, encPars);
            img.Dispose();
            img = null;

            //move tmp file into real file
            System.IO.File.Delete(Path);
            System.IO.File.Move(fileNameTmp, Path);
        }

        /// <summary>
        /// helping method for the rotaiton encoder
        /// </summary>
        /// <param name="mimeType">type of image</param>
        /// <returns>Codec info for the encoder</returns>
        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        /// <summary>
        /// Helping Method for raising the PropertyChanged event
        /// </summary>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        /// <summary>
        /// helping method to quickly raise the event with the given property name
        /// </summary>
        /// <param name="propertyName">name of the property that changed</param>
        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region eventCallbacks

        /* this is empty */

        #endregion
    }
}
