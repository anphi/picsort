﻿using System;
using System.Drawing;

namespace PicSort.File
{
    /// <summary>
    /// represents a target.
    /// inherits from TagBase
    /// </summary>
    public class Target : TagBase
    {
        #region fields

        /// <summary>
        /// Counter for numbering purposes
        /// </summary>
        private new int _Counter = 0;

        #endregion

        #region properties

        /// <summary>
        /// specifies the type of the target
        /// </summary>
        public TargetType TargetType { get; protected set; }

        #endregion

        #region events

        /* this is empty */

        #endregion

        #region constructor

        /// <summary>
        /// Constructor of a target
        /// </summary>
        /// <param name="name">name of the target; used for identification</param>
        /// <param name="path">path of the target. specifies where the files are moved to</param>
        /// <param name="token">abbreviation of the target shown in the sidebar</param>
        /// <param name="customString">custom string for labelling</param>
        /// <param name="color">color of the target in the gui</param>
        /// <param name="type">specifies the type of the target</param>
        public Target(string name, string path, string token = "", string customString = "", 
            Color color = default(Color), TargetType type = TargetType.noRename) : 
            base(name, token, customString, color)
        {

            Path = path;
            TargetType = type;
            if (token == "") Token = Name.Substring(0, 4);
            else Token = token;
        }

        #endregion

        #region destruktor

        /* this is empty */

        #endregion

        #region methods

        /// <summary>
        /// returns a string ready to insert into a filename
        /// </summary>
        /// <param name="fileName">filename to be modified, is supplied from a filerep</param>
        /// <param name="time">time to insert if needed</param>
        /// <returns>the modified filename</returns>
        public override string ReturnString(string fileName, DateTime time = default(DateTime))
        {
            if (fileName == "") throw new InvalidOperationException("can't rename an empty string");
            string ext = fileName.Substring(fileName.LastIndexOf('.'));
            switch (TargetType)
            {

                case TargetType.noRename:
                    return Path + fileName;
                case TargetType.counter:
                    string tmp = _Counter.ToString().PadLeft(Settings.Default.leadingZeroes, '0');
                    _Counter++;
                    return Path + tmp + ext;
                case TargetType.timestamp:
                    return Path + time.ToString(Settings.Default.timeFormatTarget) + ext;
                case TargetType.customString:
                    string tmp2;
                    //int protection = 0;
                    do
                    {
                        tmp2 = Path + CustomString + _Counter.ToString().PadLeft(Settings.Default.leadingZeroes, '0') + ext;
                        _Counter++;
                        if (_Counter > 50000) throw new IndexOutOfRangeException("Fehler im Verzeichnis! Zu hohe Zahl" + _Counter.ToString());
                    } while (System.IO.File.Exists(tmp2));

                    return tmp2;
                default:
                    return Path + "ERR.err";


            }
        }

        #endregion

        #region helpingMethods

        /* this is empty */

        #endregion

        #region eventCallBacks

        /* this is empty */

        #endregion

        #region interface

        /* this is empty */

        #endregion
    }
}
