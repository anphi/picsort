﻿using System;
using System.Drawing;

namespace PicSort.File
{
    /// <summary>
    /// base class for tags and targets
    /// </summary>
    public abstract class TagBase
    {
        #region fields

        /// <summary>
        /// counter for numbering purposes
        /// </summary>
        protected int _Counter = 0;

        #endregion

        #region properties

        /// <summary>
        /// color that should be displayed in the gui
        /// </summary>
        public Color Color { get; protected set; }

        /// <summary>
        /// custom string that should be processed into the fileNames
        /// </summary>
        public string CustomString { get; protected set; }

        /// <summary>
        /// abbreviation shown in the sidebar
        /// </summary>
        public string Token { get; protected set; }

        /// <summary>
        /// name of the TagBase. used for identification
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// path where files should be moved to
        /// Not necessary for tags
        /// </summary>
        public string Path { get; protected set; }

        #endregion

        #region events

        /* this is empty */

        #endregion

        #region constructor
        
        /// <summary>
        /// constructor of the TagBase
        /// </summary>
        /// <param name="name">name of the tagBase</param>
        /// <param name="token">abbreviation of the tagBase</param>
        /// <param name="customString">custom string displayed in filenames</param>
        /// <param name="color">color shown in the gui</param>                        
        public TagBase(string name, string token, string customString, Color color)
        {
            Name = name;
            Token = token;
            CustomString = customString;
            Color = color;
        }

        #endregion

        #region destruktor

        /* this is empty */


        #endregion

        #region methods
        /// <summary>
        /// returns a string for inserting it into a file name
        /// </summary>
        /// <param name="inputString">string to base the text of</param>
        /// <param name="time">time to process into the filename</param>
        /// <returns>string ready to insert</returns>
        public abstract string ReturnString(string inputString="", DateTime time = default(DateTime));

        #endregion

        #region helpingMethods

        /* this is empty */


        #endregion

        #region eventCallBacks

        /* this is empty */


        #endregion

        #region interface

        /* this is empty */


        #endregion

    }
}
