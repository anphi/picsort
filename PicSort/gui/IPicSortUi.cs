﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicSort
{
    public interface IPicSortUi
    {
        /// <summary>
        /// öffnet den FolderBrowserDialog der GUI
        /// </summary>
        /// <returns>gibt des ausgewählten Pfad zurück</returns>
        string ShowFolderDialog();


        /// <summary>
        /// öffnet eine Msg Box, um eine Fehlermeldung auszugeben
        /// </summary>
        /// <param name="ErrMsg">Fehlermeldung</param>
        void ShowError(string ErrMsg);

        /// <summary>
        /// öffnet eine Msg Box, um eine Warnung anzuzeigen
        /// </summary>
        /// <param name="WarnMsg">Warnung</param>
        void ShowWarning(string WarnMsg);

        /// <summary>
        /// iniates the creation of a new TagBase
        /// </summary>
       // void newTagDialog();

        /// <summary>
        /// opens up an window to edit Keybinds
        /// </summary>
        void EditKeyBinds();

        /// <summary>
        /// öffnet MsgBox um Info anzuzeigen
        /// </summary>
        /// <param name="Msg">Nachricht</param>
        void ShowMsg(string Msg);

        /// <summary>
        /// vergrößert das aktuelle Bild
        /// </summary>
        void ZoomIn();

        /// <summary>
        /// verkleinert das aktuelle Bild
        /// </summary>
        void ZoomOut();

        /// <summary>
        /// passt das aktuelle Bild an das Anzeigefenster an
        /// </summary>
        void FitToScreen();

        /// <summary>
        /// Stellt das Bild in seiner originalen Größe dar
        /// </summary>
        void OrigSize();

        /// <summary>
        /// Öffnet einen Dialog, den der Nutzer mit Ja oder Nein beantworten kann
        /// </summary>
        /// <returns>true, wenn ja gewählt wurde, ansonsten Nein</returns>
        bool ShowYesNoDialog(string question);
    }
}
