﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using PicSort;
using PicSort.File;

namespace PicSort.gui
{
    public class KeyStrokeHandler
    {
        #region fields
        protected UiLogic _Ui;
        #endregion

        #region properties
        public Dictionary<Keys, KeyBindDescriptor> KeyBindList { get; protected set; }
        #endregion

        #region events

        /* this is empty */

        #endregion

        #region constructor
        public KeyStrokeHandler(UiLogic pUi)
        {
            KeyBindList = new Dictionary<Keys, KeyBindDescriptor>();
            _Ui = pUi;
        }
        #endregion

        #region destruktor

        /* this is empty */

        #endregion

        #region methods

        public void HandleKeyStroke(object sender, KeyEventArgs e)
        {
            try
            {
                KeyBindList[e.KeyCode].Method?.Invoke();

            }
            catch (KeyNotFoundException)
            {

            }
        }

        public void RegisterBind(Keys key, KeyBindDescriptor desc)
        {
            if (KeyBindList.ContainsKey(key)) KeyBindList[key] = desc;
            else KeyBindList.Add(key, desc);
        }

        public void RegisterBind(Keys key, KeyStrokeAction method, GuiActions type, TagBase tagBase = null)
        {
            RegisterBind(key, new KeyBindDescriptor(type, method, tagBase));
        }

        public void DeleteBind(Keys key)
        {
            if (KeyBindList.ContainsKey(key)) KeyBindList.Remove(key);
        }

        public KeyStrokeAction ReturnDefaultActionDelegate(GuiActions action)
        {
            switch (action)
            {
                case GuiActions.openFolder:
                    return new KeyStrokeAction(() => _Ui.LoadFolderAsync(_Ui.UiHost.ShowFolderDialog()));
                case GuiActions.openFile:
                    return new KeyStrokeAction(() => _Ui.LoadFile(_Ui.UiHost.ShowFolderDialog()));
                case GuiActions.delete:
                    return new KeyStrokeAction(() => _Ui.DeleteFile());
                case GuiActions.rotateLeft:
                    return new KeyStrokeAction(() => _Ui.Rotate(Orientation.left));
                case GuiActions.rotateRight:
                    return new KeyStrokeAction(() => _Ui.Rotate(Orientation.right));
                case GuiActions.rotateOverHead:
                    return new KeyStrokeAction(() => _Ui.Rotate(Orientation.head));
                case GuiActions.nextFile:
                    return new KeyStrokeAction(() => _Ui.NextFile());
                case GuiActions.previousFile:
                    return new KeyStrokeAction(() => _Ui.PreviousFile());
                case GuiActions.executeQueue:
                    return new KeyStrokeAction(() => _Ui.ExecuteAsync());
                case GuiActions.TagTarget:
                    throw new ArgumentException("Nur auf Default Methoden anwendbar");
                case GuiActions.undo:
                    return new KeyStrokeAction(() => _Ui.Undo());
                case GuiActions.redo:
                    return new KeyStrokeAction(() => _Ui.Redo());
                case GuiActions.zoomIn:
                    return new KeyStrokeAction(() => _Ui.UiHost.ZoomIn());
                case GuiActions.zoomOut:
                    return new KeyStrokeAction(() => _Ui.UiHost.ZoomOut());
                case GuiActions.fitToScreen:
                    return new KeyStrokeAction(() => _Ui.UiHost.FitToScreen());
                case GuiActions.originalSize:
                    return new KeyStrokeAction(() => _Ui.UiHost.OrigSize());
                default:
                    throw new Exception("ParsingError");
            }
        }

        /// <summary>
        /// writes a summary of all Keybinds to a text file
        /// </summary>
        /// <param name="path"></param>
        public void WriteKeysToFile(string path)
        {
            List<string> binds = new List<string>();
            foreach (KeyValuePair<Keys, KeyBindDescriptor> bind in KeyBindList)
            {
                if (bind.Value.ActionType == GuiActions.TagTarget) binds.Add(bind.Key.ToString() + " - Zuweisung:" + bind.Value.KeyBindTag.Name);
                else binds.Add(bind.Key.ToString() + " - " + bind.Value.ActionType.ToString());
            }
            try { System.IO.File.WriteAllLines(path, binds); }
            catch (IOException) { _Ui.LogError("KeystrokeHandler", "Fehler beim schreiben der Keybind Liste", true); }
        }

        #endregion

        #region helpingMethods

        /* this is empty */

        #endregion

        #region eventCallBacks

        /* this is empty */

        #endregion

        #region interface

        /* this is empty */

        #endregion
    }
}
