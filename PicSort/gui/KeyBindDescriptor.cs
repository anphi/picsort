﻿using System;
using System.Collections.Generic;
using System.Linq;
using PicSort;
using PicSort.File;

namespace PicSort.gui
{
    public class KeyBindDescriptor
    {
        #region Felder
        #endregion

        #region Properties
        public KeyStrokeAction Method { get; protected set; }
        public GuiActions ActionType { get; protected set; }
        public TagBase KeyBindTag { get; protected set; }
        #endregion

        #region Events
        #endregion

        #region Konstruktor
        public KeyBindDescriptor(GuiActions type, KeyStrokeAction method, TagBase tagBase=null)
        {
            Method = method;
            ActionType = type;
            KeyBindTag = tagBase; 
        }
        #endregion

        #region Destruktor
        #endregion

        #region Methoden
        #endregion

        #region Hilfsmethoden
        #endregion

        #region Ereignismethoden
        #endregion

    }
}
