﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Drawing;

using PicSort.File;
using PicSort.gui;
using PicSort.xml;
using System.Threading.Tasks;

namespace PicSort
{
    public class UiLogic
    {
        #region Felder
        private int _OldIndex;
        protected bool _Notified;
        internal IPicSortUi UiHost;
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Properties
        public int ImageCount { get { return files.Count; } }
        public int ImageHeight
        {
            get
            {
                if (ImageCount > 0)
                {
                    _Notified = false; //make sure user gets only notified once, not once per picture
                    return files.ActiveFile.Height;
                }
                else if (!_Notified)
                {
                    _Notified = true;
                    ShowNoFileErr("UiLogic");
                    return 0;
                }
                else return 0;
            }
        }
        public int ImageWidth
        {
            get
            {
                if (ImageCount > 0)
                {
                    _Notified = false;
                    return files.ActiveFile.Width;
                }
                else if (!_Notified)
                {
                    _Notified = true;
                    ShowNoFileErr("UiLogic");
                    return 0;
                }
                else return 0;
            }
        }
        public int PicsToDelete { get; protected set; }
        public int SelectedRowIndex { get { return files.FileIndex; } }
        public int UnsortedPics { get; protected set; }
        public int Progress { get { return files.Progress; } }

        public string CurrentFolder { get; protected set; }

        public Image CurrentImage { get; protected set; }
        public DataGridViewCellStyle[,] Styles { get; protected set; }
        public DataTable UiDataTable { get { return GenTable(); } }
        public List<TagBase> TagList { get; protected set; }

        public bool SMoveFileToTrashcan
        {
            get { return Settings.Default.actMoveFileToTrash; }
            set
            {
                Settings.Default.actMoveFileToTrash = value;
                SaveSettings();
            }
        }
        public int SLeadingZeros
        {
            get { return Settings.Default.leadingZeroes; }
            set
            {
                Settings.Default.leadingZeroes = value;
                SaveSettings();
            }
        }
        public string SPathLastFolder
        {
            get { return Settings.Default.pathLastUsed; }
            set
            {
                Settings.Default.pathLastUsed = value;
                SaveSettings();
            }
        }
        public string SPathLastXml
        {
            get { return Settings.Default.pathLastXml; }
            set
            {
                Settings.Default.pathLastXml = value;
                SaveSettings();
            }
        }
        public string SPathStartUpXml
        {
            get { return Settings.Default.pathStartupXml; }
            set
            {
                Settings.Default.pathStartupXml = value;
                SaveSettings();
            }
        }
        public string SPathStartUpFolder
        {
            get { return Settings.Default.pathStartUpFolder; }
            set
            {
                Settings.Default.pathStartUpFolder = value;
                SaveSettings();
            }
        }
        public string SPathTargetDefault
        {
            get { return Settings.Default.pathTargetDefault; }
            set
            {
                Settings.Default.pathTargetDefault = value;
                SaveSettings();
            }
        }
        public bool SSaveTagTargetWithEnter
        {
            get { return Settings.Default.actSaveTagTargetWithEnter; }
            set
            {
                Settings.Default.actSaveTagTargetWithEnter = value;
                SaveSettings();
            }
        }
        public bool SStartupImportXml
        {
            get { return Settings.Default.startImportXml; }
            set
            {
                Settings.Default.startImportXml = value;
                SaveSettings();
            }
        }
        public bool SStartupImportFolder
        {
            get { return Settings.Default.startImportFolder; }
            set
            {
                Settings.Default.startImportFolder = value;
                SaveSettings();
            }
        }
        public bool SSkipOnAssignment
        {
            get { return Settings.Default.skipOnAssignment; }
            set { Settings.Default.skipOnAssignment = value; }
        }
        public bool SSkipOnDelete
        {
            get { return Settings.Default.skipOnDelete; }
            set
            {
                Settings.Default.skipOnDelete = value;
                SaveSettings();
            }
        }
        public string STimeStampTag
        {
            get { return Settings.Default.timeFormatTag; }
            set
            {
                Settings.Default.timeFormatTag = value;
                SaveSettings();
            }
        }
        public string STimeStampTarget
        {
            get { return Settings.Default.timeFormatTarget; }
            set
            {
                Settings.Default.timeFormatTarget = value;
                SaveSettings();
            }
        }
        public int SScrollCount
        {
            get { return Settings.ScrollCount; }
            set { Settings.ScrollCount = value; SaveSettings(); }
        }

        internal FileList files { get; private set; }
        internal KeyStrokeHandler KeyStrokeHandler { get; private set; }
        internal XmlHandler Xml { get; private set; }
        internal Settings Settings { get; private set; }
        internal Os Runtime { get; private set; }

        #endregion

        #region Events
        public event EventHandler NewTableReady;
        public event EventHandler NewPictureReady;
        public event EventHandler LoadingStarted;
        public event EventHandler LoadingFinished;
        public event EventHandler NewProgress;
        public event TagBaseEventHandler TagBaseNewEvent;
        public event TagBaseEventHandler TagBaseChangedEvent;
        public event TagBaseEventHandler TagBaseDeletedEvent;
        #endregion

        #region Konstruktor

        public UiLogic(IPicSortUi ui, Os runtime)
        {
            switch (Runtime)
            {
                case Os.win:
                    files = new FileList('\\');
                    break;
                case Os.linux:
                    files = new FileList('/');
                    break;
                default:
                    throw new ArgumentException("Geben sie Linux oder Win an");

            }
            TagList = new List<TagBase>();
            KeyStrokeHandler = new KeyStrokeHandler(this);
            UiHost = ui;
            Runtime = runtime;
            Settings = new Settings();
            files.NewProgress += Files_NewProgress;


        }


        #endregion

        #region Destruktor
        #endregion

        #region Methoden
        //Datei Aktionen
        public void LoadFile(string path)
        {
            files.LoadFile(path);
            InvokeNewTableReady();
        }

        public async void LoadFolderAsync(string path)
        {
            LoadingStarted?.Invoke(this, new EventArgs());
            CurrentFolder = path;
            if (Directory.EnumerateFiles(CurrentFolder).Count() > 0)
            {
                try
                {
                    await Task.Run(() => files.LoadFolder(path));
                    SetFileIndex(0);

                    LoadingFinished?.Invoke(this, new EventArgs());
                }
                catch (IOException ex)
                {
                    LogError("files", "IOException: " + ex.Message);
                    UiHost.ShowError("IOExecption mit folgender Fehlermeldung aufgetreten: " + ex.Message);
                }
            }

            else
            {
                LogError("files", "der zu ladende Ordner ist leer");
                UiHost.ShowError("Der angegebene Ordner ist leer");
            }
            InvokeNewTableReady();
            LogInfo("FileLoader", "Loaded " + files.Count.ToString() + " Files");
        }

        public void ReloadFolder()
        {
            files.Clear();
            LoadFolderAsync(CurrentFolder);
        }

        public void NextFile()
        {
            if (ImageCount > 0)
            {
                files.NextFile();
                using (var bitmap = new Bitmap(files.ActiveFile.Path))
                {
                    CurrentImage = new Bitmap(bitmap);
                }
                NewPictureReady?.Invoke(this, new EventArgs());
            }
            else ShowNoFileErr("Navigation");
        }

        public void PreviousFile()
        {
            if (ImageCount > 0)
            {
                files.PreviousFile();
                using (var bitmap = new Bitmap(files.ActiveFile.Path))
                {
                    CurrentImage = new Bitmap(bitmap);
                }
                NewPictureReady?.Invoke(this, new EventArgs());
            }
            else ShowNoFileErr("Navigation");
        }

        public void SetFileIndex(int index)
        {
            if (files.Count > 0)
            {
                files.SetFileIndex(index);
                using (var bitmap = new Bitmap(files.ActiveFile.Path))
                {
                    CurrentImage = new Bitmap(bitmap);
                }
                NewPictureReady?.Invoke(this, new EventArgs());
            }
            else ShowNoFileErr("Navigation");

        }

        public void DeleteFile()
        {
            if (ImageCount > 0)
            {
                files.ActiveFile.Delete();
                PicsToDelete++;
                InvokeNewTableReady();
                if (Settings.Default.skipOnDelete) NextFile();
            }
            else ShowNoFileErr("Navigation");
        }

        public void Rotate(Orientation orientation)
        {
            if (ImageCount > 0)
            {
                files.ActiveFile.Rotate(orientation);
                InvokeNewTableReady();
            }
            else ShowNoFileErr("Navigation");
        }

        public void AssignTagBase(TagBase tagBase)
        {
            if (ImageCount > 0)
            {
                if (tagBase.GetType() == typeof(Tag))
                {
                    _OldIndex = files.FileIndex;
                    files.ActiveFile.AssignTag((Tag)tagBase);
                    InvokeNewTableReady();
                    SetFileIndex(_OldIndex);
                }

                else if (tagBase.GetType() == typeof(Target))
                {
                    _OldIndex = files.FileIndex;
                    files.ActiveFile.AssignTarget((Target)tagBase);
                    InvokeNewTableReady();
                    SetFileIndex(_OldIndex);
                }
                if (Settings.Default.skipOnAssignment && typeof(Target) == tagBase.GetType()) NextFile();
            }
            else ShowNoFileErr("TagAssignment");
        }

        public void Undo()
        {
            if (ImageCount > 0)
            {
                try
                {
                    files.ActiveFile.Undo();
                    if (!files.ActiveFile.IsDeleted && PicsToDelete > 0) PicsToDelete--;
                    InvokeNewTableReady();
                }
                catch (IndexOutOfRangeException)
                {
                    UiHost.ShowWarning("Es gibt nichts rückgängig zu machen");
                }

            }
            else ShowNoFileErr("undo");
        }

        public void Redo()
        {
            if (ImageCount > 0)
            {
                try
                {
                    files.ActiveFile.Redo();
                    if (files.ActiveFile.IsDeleted) PicsToDelete++;
                    InvokeNewTableReady();
                }
                catch (IndexOutOfRangeException)
                {
                    UiHost.ShowWarning("es gibt nicht wiederherzustellen");
                }

            }
            else ShowNoFileErr("redo");
        }

        public async void ExecuteAsync()
        {
            if (ImageCount > 0)
            {
                LogInfo("files", "Starting Execution");
                await Task.Run(() => files.Execute());

                PicsToDelete = 0;
                ReloadFolder();
                UnsortedPics = ImageCount;
            }
            else ShowNoFileErr("execute");
        }

        public void Clear()
        {
            files.Clear();
            InvokeNewTableReady();
        }

        public void Reset()
        {
            foreach (FileRep f in files)
            {
                f.Reset();
            }
            InvokeNewTableReady();
        }

        //Targets und Tags
        public void AddTagBase()
        {
            //uiHost.
        }

        public void AddTagBase(TagBase tagBase)
        {
            bool createFolder = true;
            if (tagBase.GetType() == typeof(Target) && !Directory.Exists(tagBase.Path))
            {
                createFolder = UiHost.ShowYesNoDialog("Der gewählte Ordner existiert nicht. Soll er erstellt werden?");
                if (createFolder) Directory.CreateDirectory(tagBase.Path);
            }
            if (tagBase.GetType() == typeof(Tag) || createFolder) // always add tags since they have no path, otherwise check if the path exists
            {
                TagList.Add(tagBase);
                TagBaseNewEvent?.Invoke(this, new TagBaseEventArgs(tagBase));
            }
            else LogWarning("tags", "Ordner " + tagBase.Path + " wurde nicht gefunden, Tagerstellung abgebrochen", true);
        }

        public void EditTagBase(string oldName, TagBase newTagBase)
        {
            try
            {
                TagList[TagList.IndexOf(SearchTagBase(oldName))] = newTagBase;
                TagBaseChangedEvent?.Invoke(this, new TagBaseEventArgs(newTagBase));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler:" + ex.Message);
            }
        }

        public void DeleteTagBase(TagBase tagBase)
        {
            TagList.Remove(tagBase);
            TagBaseDeletedEvent?.Invoke(this, new TagBaseEventArgs(tagBase));

        }

        public TagBase ReturnTagBase(string name)
        {
            return SearchTagBase(name);
        }

        public void clearTagBases()
        {
            while (TagList.Count > 0)
            {
                DeleteTagBase(TagList[0]);
            }
        }


        //Keybinds
        public void EditKeybinds()
        {
            UiHost.EditKeyBinds();
        }

        public void HandleKeyStroke(object sender, KeyEventArgs e)
        {
            KeyStrokeHandler.HandleKeyStroke(sender, e);
        }

        public void KeyBindUpdate(Keys key, KeyBindDescriptor desc)
        {
            KeyStrokeHandler.RegisterBind(key, desc);
        }

        public void KeyBindUpdate(Keys key, KeyStrokeAction method, GuiActions type, TagBase tagBase = null)
        {
            KeyBindUpdate(key, new KeyBindDescriptor(type, method, tagBase));
        }

        public void KeyBindDelete(Keys key)
        {
            KeyStrokeHandler.DeleteBind(key);
        }

        public Dictionary<Keys, KeyBindDescriptor> ReturnKeyBinds()
        {
            return KeyStrokeHandler.KeyBindList;
        }

        public KeyStrokeAction ReturnDefaultActionDelegate(GuiActions action)
        {
            return KeyStrokeHandler.ReturnDefaultActionDelegate(action);
        }

        public void ExportKeybinds(string path)
        {
            KeyStrokeHandler.WriteKeysToFile(path);
        }

        //XML
        public void StoreXML(string path)
        {
            bool enableAutoLoad = UiHost.ShowYesNoDialog("Soll die Datei beim Start automatisch geladen werden?");
            if (enableAutoLoad)
            {
                Settings.startImportXml = true;
                Settings.pathStartupXml = path;
                Settings.Save();
            }
            Xml = new XmlHandler(this, path);
            Xml.WriteTagList(TagList);
            Xml.WriteKeybinds(KeyStrokeHandler.KeyBindList);
            Xml.WriteSettings();
        }

        public void LoadXML(string path)
        {
            if (System.IO.File.Exists(path))
            {
                try
                {
                    clearTagBases();
                    Xml = new XmlHandler(this, path);
                    Xml.LoadTagList();
                    Xml.LoadKeyBinds();
                    Xml.LoadSettings();
                    LogInfo("xml", "XML erfolgreich importiert", true);
                    LogInfo("xml", "imported XML file: " + path);
                }
                catch (Exception ex)
                {
                    UiHost.ShowError(ex.Message);
                }
            }
            else LogError("xml", "XML Datei: " + path + " nicht gefunden, benutze Default Datei", true);
        }

        public void AddTagBasefromXml(string path)
        {
            if (System.IO.File.Exists(path))
            {
                Xml = new XmlHandler(this, path);
                Xml.LoadTagList();
                LogInfo("xml", "Tags erfolgreich importiert", true);
                LogInfo("xml", "imported XML file: " + path);
            }
            else LogError("xml", "XML Datei: " + path + " nicht gefunden, benutze Default Datei", true);

        }

        //Verwaltung
        public void FinishStartup()
        {
            HandleStartUp();
        }

        //Settings
        public void SaveSettings()
        {
            Settings.Default.Save();
        }

        //logging
        public void LogDebug(string sender, string message)
        {
            _Logger.Debug('[' + sender + ']' + message);
        }

        public void LogInfo(string sender, string message, bool showMessage = false)
        {
            _Logger.Info('[' + sender + ']' + message);
            if (showMessage) UiHost.ShowMsg(message);
        }

        public void LogWarning(string sender, string message, bool showMessage = false)
        {
            _Logger.Warn('[' + sender + ']' + message);
            if (showMessage) UiHost.ShowWarning(message);
        }

        public void LogError(string sender, string message, bool showMessage = false)
        {
            _Logger.Error('[' + sender + ']' + message);
            if (showMessage) UiHost.ShowError(message);
        }

        public void LogFatal(string sender, string message, bool showMessage = false)
        {
            _Logger.Fatal('[' + sender + ']' + message);
            if (showMessage) UiHost.ShowError(message);
        }


        #endregion

        #region Hilfsmethoden
        private DataTable GenTable()
        {
            DataTable tbl = new DataTable();
            tbl.Columns.Add("Dateiname");
            tbl.Columns.Add("Aktion");
            tbl.Columns.Add("Tag");
            tbl.Columns.Add("Target");

            if (files.Count == 0) return tbl;

            Styles = new DataGridViewCellStyle[files.Count, 3];
            for (int y = 0; y < files.Count; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    Styles[y, x] = new DataGridViewCellStyle();
                }
            }

            for (int i = 0; i < files.Count; i++)
            {
                FileRep f = files[i];

                DataRow row = tbl.NewRow();
                row["Dateiname"] = f.FileName;
                if (f.Tag != null) row["Tag"] = f.Tag.Token;
                if (f.Target != null) row["Target"] = f.Target.Token;


                string str = "";
                if (f.IsDeleted) str += "del, ";
                if (f.Orientation == Orientation.left) str += "-90°, ";
                else if (f.Orientation == Orientation.right) str += "+90°, ";
                else if (f.Orientation == Orientation.head) str += "+180°";
                row["Aktion"] = str;

                if (f.IsDeleted) Styles[i, 0].Font = new Font("Calibri", 9, FontStyle.Strikeout);
                if (f.Tag != null) Styles[i, 1].BackColor = f.Tag.Color;
                if (f.Target != null) Styles[i, 2].BackColor = f.Target.Color;

                tbl.Rows.Add(row);
            }
            return tbl;
        }

        private TagBase SearchTagBase(string name)
        {
            try
            {
                return TagList.Single(p => p.Name == name);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler:" + ex.Message);
                return null;
            }
        }

        private void HandleStartUp()
        {
            if (Settings.Default.startImportXml) LoadXML(Settings.Default.pathStartupXml);
            if (Settings.Default.startImportFolder) LoadFolderAsync(Settings.Default.pathStartUpFolder);
        }

        private void ShowNoFileErr(string sender)
        {
            LogError(sender, "Keine Dateien geladen!", true);
        }

        private void InvokeNewTableReady()
        {
            UnsortedPics = 0;
            foreach (FileRep f in files)
            {
                if (!f.NeedsExecution) UnsortedPics++;
            }
            this.NewTableReady?.Invoke(this, new EventArgs());
        }

        private void Files_NewProgress(object sender, EventArgs e)
        {
            NewProgress?.Invoke(this, new EventArgs());
        }

        #endregion

    }
}
