﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Forms;
using System.Configuration;
using System.Linq;

using PicSort.File;
using PicSort.gui;


namespace PicSort.xml
{
    internal class XmlHandler
    {
        #region Felder
        protected XDocument _Doc;
        protected UiLogic _Ui;
        #endregion

        #region Properties
        public string Path { get; protected set; }
        #endregion

        #region Events
        #endregion

        #region Konstruktor
        public XmlHandler(UiLogic ui, string path)
        {
            try
            {
                _Ui = ui;
                Path = path;
                if (!System.IO.File.Exists(path)) { System.IO.File.WriteAllText(path, Properties.Resources.DefaultXml); }
                _Doc = XDocument.Load(path);

            }

            catch (XmlException ex)
            {
                _Ui.UiHost.ShowError("Fehler beim Lesen der XML Datei: " + Path + " Ist die Datei beschädigt?");
            }
        }
        #endregion

        #region Destruktor
        #endregion

        #region Methoden
        public void WriteTagList(List<TagBase> pList)
        {
            _Doc.Element("root").Element("tags").RemoveAll(); // clear all Tags
            foreach (TagBase tagBase in pList)
            {
                if (tagBase.GetType() == typeof(Tag))
                {
                    Tag tmp = (Tag)tagBase;
                    _Doc.Element("root").Element("tags").Add(new XElement("tag",
                       new XElement("color", tmp.Color.ToArgb()),
                       new XElement("customString", tmp.CustomString),
                       new XElement("kuerzel", tmp.Token),
                       new XElement("name", tmp.Name),
                       new XElement("tagPosition", tmp.TagPosition),
                       new XElement("tagType", tmp.TagType)
                       )
                    );
                }

                else if (tagBase.GetType() == typeof(Target))
                {
                    Target tmp = (Target)tagBase;
                    _Doc.Element("root").Element("tags").Add(new XElement("target",
                       new XElement("color", tmp.Color.ToArgb()),
                       new XElement("customString", tmp.CustomString),
                       new XElement("kuerzel", tmp.Token),
                       new XElement("name", tmp.Name),
                       new XElement("path", tmp.Path),
                       new XElement("targetType", tmp.TargetType)
                       )
                     );
                }
            }
            
            _Doc.Save(Path);

        }

        public void WriteKeybinds(Dictionary<Keys, KeyBindDescriptor> list)
        {
            _Doc.Element("root").Element("keybinds").RemoveAll(); // clear keybinds
            foreach (KeyValuePair<Keys,KeyBindDescriptor> pair in list)
            {
                if (pair.Value.ActionType == GuiActions.TagTarget)
                {
                    _Doc.Element("root").Element("keybinds").Add(new XElement("key", new XAttribute("key", pair.Key),
                                new XElement("actionType", pair.Value.ActionType),
                                new XElement("tagName", pair.Value.KeyBindTag.Name)

                                ));
                }
                else
                {
                    _Doc.Element("root").Element("keybinds").Add(new XElement("key", new XAttribute("key", pair.Key),
                                new XElement("actionType", pair.Value.ActionType)
                                ));
                }
            }
            _Doc.Save(Path);
        }

        public void WriteSettings()
        {
            _Doc.Element("root").Element("settings").RemoveAll(); // clear all settings
            foreach (SettingsPropertyValue prop in Settings.Default.PropertyValues)
            {
                _Doc.Element("root").Element("settings").Add(new XElement("property",
                            new XAttribute("name", prop.Name),
                            new XAttribute("type", Settings.Default.Properties[prop.Name].PropertyType.ToString()),
                            prop.SerializedValue.ToString()   
                    ));
            }
            var sorted = _Doc.Element("root").Element("settings").Elements("property").OrderBy(s => (string)s.Attribute("name"));
            var arr = sorted.ToArray<XElement>(); //copy so it doesnt get modified when removing the unordered ones
            _Doc.Element("root").Element("settings").RemoveAll();
            _Doc.Element("root").Element("settings").Add(arr);
            _Doc.Save(Path);
        }

        public void LoadTagList()
        {
            try
            {
                foreach (XElement tag in _Doc.Element("root").Element("tags").Elements())
                {
                    if (tag.Name == "tag")
                    {
                        Tag tmp = new Tag(
                            tag.Element("name").Value,
                            tag.Element("kuerzel").Value,
                            tag.Element("customString").Value,
                            System.Drawing.Color.FromArgb(Convert.ToInt32(tag.Element("color").Value)),
                            (TagPosition)Enum.Parse(typeof(TagPosition), tag.Element("tagPosition").Value, true),
                            (TagType)Enum.Parse(typeof(TagType), tag.Element("tagType").Value, true)
                            );

                        if (!_Ui.TagList.Exists(t => t.Name == tmp.Name)) _Ui.AddTagBase(tmp);
                    }

                    else if (tag.Name == "target")
                    {
                        TargetType cmp = (TargetType)Enum.Parse(typeof(TargetType), tag.Element("targetType").Value, true);
                        Target tmp;
                        if (cmp == TargetType.customString)
                        {

                            tmp = new Target(
                                           tag.Element("name").Value,
                                           tag.Element("path").Value,
                                           tag.Element("kuerzel").Value,
                                           tag.Element("customString").Value,
                                           System.Drawing.Color.FromArgb(Convert.ToInt32(tag.Element("color").Value)),
                                           cmp
                                           );
                        }
                        else
                        {
                            tmp = new Target(
                                         tag.Element("name").Value,
                                         tag.Element("path").Value,
                                         tag.Element("kuerzel").Value,
                                         tag.Element("customString").Value,
                                         System.Drawing.Color.FromArgb(Convert.ToInt32(tag.Element("color").Value)),
                                         cmp
                                         );
                        }
                        if (!_Ui.TagList.Exists(t => t.Name == tmp.Name)) _Ui.AddTagBase(tmp); //check for same Name


                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public void LoadKeyBinds()
        {
            foreach (XElement bind in _Doc.Element("root").Element("keybinds").Elements())
            {
                GuiActions actionType = (GuiActions) Enum.Parse(typeof(GuiActions), bind.Element("actionType").Value, true);
                Keys key = (Keys) Enum.Parse(typeof(Keys),bind.Attribute("key").Value,true);
                KeyBindDescriptor desc;
                if (actionType == GuiActions.TagTarget)
                {
                    string tagName = bind.Element("tagName").Value;
                    TagBase tagBase = _Ui.TagList.Find(p => p.Name == tagName);
                    desc = new KeyBindDescriptor(actionType, () => _Ui.AssignTagBase(tagBase), tagBase);
                }

                else
                {
                    desc = new KeyBindDescriptor(actionType, _Ui.ReturnDefaultActionDelegate(actionType));
                }
                _Ui.KeyBindUpdate(key, desc);
            }
        }

        public void LoadSettings()
        {
            try
            {
                foreach (var prop in _Doc.Element("root").Element("settings").Elements())
                {
                    Type t = Type.GetType(prop.Attribute("type").Value);
                    Settings.Default[prop.Attribute("name").Value] = Convert.ChangeType(prop.Value, t);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        #endregion

        #region Hilfsmethoden
        
        #endregion

        #region Ereignismethoden
        #endregion
    }
}
